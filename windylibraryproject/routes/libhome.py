from flask import render_template, request, session

from windylibraryproject import app, db
from windylibraryproject.routes.loginform import LoginForm
from windylibraryproject.routes.resgisterform import RegistrationForm
from windylibraryproject.model.loginmodel import Login
from windylibraryproject.model.customermodel import Customer
from windylibraryproject.model.addressmodel import Address
from windylibraryproject.model.authormodel import Author
from windylibraryproject.model.bookmodel import Book
from windylibraryproject.routes.searchform import SearchForm
from windylibraryproject.routes.addbookform import AddBookForm
from windylibraryproject.model.mediamodel import Media
from sqlalchemy.sql import text


@app.route("/home", methods=['GET', 'POST'])
def home():
    if 'logginUser' in session:
        username = session['logginUser']
        data = "Hi Welcome " + username
        return render_template('homepage.html', value=data)
    else:
        return render_template('homepage.html')


@app.route("/contact", methods=['GET', 'POST'])
def contact():
    return render_template('contact.html')


@app.route("/register", methods=['GET', 'POST'])
def register():
    rform = RegistrationForm(request.form)
    if request.method == 'POST':
        address = Address(rform.addr1.data, rform.state.data, rform.city.data, '78730')
        customer = Customer(firstname=rform.firstname.data,
                            lastname=rform.lastname.data,
                            dob=rform.dob.data,
                            email=rform.email.data,
                            custzone=1,
                            custphone=rform.phone.data)

        login = Login(
            username=rform.firstname.data[0].lower() + rform.lastname.data.lower(),
            password=rform.password1.data)

        session["logginUser"] = rform.firstname.data[0].lower() + rform.lastname.data.lower()
        customer.login.append(login)
        address.customer.append(customer)
        db.session.add(address)
        db.session.commit()

        return home()
    return render_template('registration.html', form=RegistrationForm)


@app.route("/login", methods=['GET', 'POST'])
def login1():
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate_on_submit():
        # login = loginmodel.Login.query.filter_by(username=form.username.data, password=form.password.data).first()
        # session.__setattr__("logginUser",form.username.data)
        # customer = customermodel.Customer.query.filter_by(cust_id=login.cust_id).first()
        userinputlogin = Login(form.username.data, form.password.data)
        result = db.session.query(Login, Customer).join(Customer, Login.cust_id == Customer.cust_id) \
            .filter(Login.username == userinputlogin.username, Login.password == userinputlogin.password).first()

        if result:
            session["logginUser"] = result.Customer.firstname + " " + result.Customer.lastname
            return home()
        else:
            # flash('Login Unsuccessful. Please check email and password', 'danger')
            session.pop("logginUser")
            return "Login failed"
    else:
        return render_template('login.html', title="Login", form=form)


@app.route("/search", methods=['GET', 'POST'])
def serach():
    Searchdata = SearchForm(request.form)

    if request.method == 'POST':
        query = db.session.query(Book.title, Book.noofcopies, Author.authorFname, Author.authorLname). \
            join(Author, Author.ISBN == Book.ISBN) \
            .filter(Author.authorFname.like("%" + Searchdata.authorSearch.data + "%"),
                    Book.title.like("%" + Searchdata.bookSearch.data + "%"))
        searchresult = query.all()

        return render_template('search.html', len=len(searchresult), searchresult=searchresult)

    return render_template('search.html', form=Searchdata)


@app.route("/addbook", methods=['GET', 'POST'])
def addbook():
    Addbookform = AddBookForm(request.form)

    if request.method == 'POST':
        ##     bookmediaquery = db.session.query (Book.isbn,Book.title,Book.pubyear,Book.noofcopies,Book.price,Media.status).join(Media,Media.madiaid=Book.mediaid)
        media = Media(Addbookform.mediaid.data, status="1")

        book = Book(isbn=Addbookform.isbn.data, title=Addbookform.title.data, pubyear=Addbookform.pubyear.data,
                    noofcopies=Addbookform.noofcopies.data, price=Addbookform.price.data,
                    mediaid=Addbookform.mediaid.data)
        author = Author(authorid="1009", authorfirstname=Addbookform.authorfirstname.data,
                        authorlastname=Addbookform.authorlastname.data, isbn=Addbookform.isbn.data)

        media.book.append(book)
        db.session.add(media)
        db.session.add(book)
        db.session.add(author)
        db.session.commit()
        return home()
    else:
        return render_template('addbook.html', title='Add Book', form=Addbookform)

if __name__ == '__main__':
    app.run(debug=True)
