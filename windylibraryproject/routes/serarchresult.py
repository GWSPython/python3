from wtforms import StringField, IntegerField
from flask_wtf import FlaskForm

class SearchResult(FlaskForm):
    title = StringField("title")
    author= StringField ("author")
    copies = StringField ("no of copies")

    def __init__(self, title, author, copies):
        self.title = title
        self.author = author
        self.copies = copies