from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField,DateField
from wtforms.validators import DataRequired, Length

class RegistrationForm(FlaskForm):
    firstname = StringField('firstname',validators=[DataRequired(), Length(max=50)])
    lastname = StringField('lastname',validators=[DataRequired(), Length (max=10)])
    dob = DateField ('dob',validators=[DataRequired])
    email = StringField('email',validators=[DataRequired])
    addr1 = StringField ('Address1',validators=[DateField])
    addr2 = StringField ('Address2')
    city = StringField ('city',validators=[DataRequired])
    state = StringField ('state',validators=[DataRequired])
    phone = StringField ('phone',validators=[DataRequired])
    password1 = PasswordField('password1', validators=[DataRequired()])
    password2 = PasswordField('password2', validators=[DataRequired()])
    submit = SubmitField('Create Account')