from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField
from wtforms.validators import DataRequired, Length

class AddBookForm(FlaskForm):
    isbn = StringField('isbn', validators=[DataRequired])
    title = StringField('title', validators=[DataRequired])
    authorfirstname = StringField('authorfirstname',validators=[DataRequired(), Length(max=50)])
    authorlastname = StringField('authorlastname',validators=[DataRequired(), Length (max=50)])
    pubyear = IntegerField ('pubyear',validators=[DataRequired])
    noofcopies = IntegerField('noofcopies',validators=[DataRequired])
    currency=StringField('currency')
    price = IntegerField('price',validators=[DataRequired])
    mediaid = IntegerField ('mediaid',validators=[DataRequired])
