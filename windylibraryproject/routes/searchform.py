from wtforms import StringField
from flask_wtf import FlaskForm

class SearchForm(FlaskForm):
    authorSearch = StringField("authorfirstname")
    bookSearch = StringField("title")
