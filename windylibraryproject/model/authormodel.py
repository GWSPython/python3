from windylibraryproject import app, db
from windylibraryproject.model.bookmodel import Book

class Author (db.Model):
    authorid=db.Column('author_id', db.Integer, primary_key="true")
    authorfirstname = db.Column('author_first_name', db.String(50))
    authorlastname = db.Column('author_last_name', db.String(50))
    isbn = db.Column('ISBN', db.ForeignKey(Book.isbn))


    def __init__(self, authorid,authorfirstname,authorlastname,isbn):
        self.authorid=authorid
        self.authorfirstname=authorfirstname
        self.authorlastname=authorlastname
        self.isbn = isbn


    def __repr__(self):
        return '<The author of this book is %r>' % self.authorfirstname