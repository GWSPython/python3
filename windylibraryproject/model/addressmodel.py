from windylibraryproject import app, db

class Address(db.Model):
    addrstreet= db.Column('addr_street', db.String(50))
    addrcity= db.Column('addr_city', db.String(10))
    addrstate = db.Column('addr_state', db.String(10))
    addrzip = db.Column ('addr_zip',db.String(10))
    addressid = db.Column ('addr_id', db.Integer, primary_key = 'true')
    customer = db.relationship ("Customer", backref="placement_hash")

    def __init__(self, addrstreet, addrcity, addrstate, addrzip):
        self.addrstreet=addrstreet
        self.addrcity=addrcity
        self.addrstate=addrstate
        self.addrzip=addrzip

