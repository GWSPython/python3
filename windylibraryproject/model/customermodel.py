from windylibraryproject import app, db
from sqlalchemy.dialects.mysql import MEDIUMINT, TINYINT
from windylibraryproject.model.addressmodel import Address



class Customer(db.Model):
    firstname = db.Column('cust_first_name', db.String(50))
    lastname = db.Column('cust_last_name', db.String(10))
    cust_id = db.Column('cust_id', db.Integer, primary_key='true')
    dob = db.Column ('cust_dob',db.Date)
    email = db.Column ('cust_email',db.String)
    custInZone = db.Column('cust_in_zone', TINYINT)
    addr_id = db.Column(db.Integer, db.ForeignKey(Address.addressid))
    custphone = db.Column ('cust_phone',db.String(50))
    login = db.relationship("Login",backref="placement_hash")


    def __init__(self, firstname, lastname,dob,email,custzone,custphone):
        self.firstname = firstname
        self.lastname = lastname
        self.dob = dob
        self.email = email
        self.custInZone = custzone
        self.custphone = custphone

