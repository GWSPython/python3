from windylibraryproject import app, db
from windylibraryproject.model.customermodel import Customer


class Login(db.Model):
    username = db.Column('username', db.String(30), primary_key='true')
    password = db.Column('password', db.String(20))
    cust_id = db.Column('cust_id', db.ForeignKey(Customer.cust_id))

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def __repr__(self):
        return '<User smitha is %r>' % self.username
