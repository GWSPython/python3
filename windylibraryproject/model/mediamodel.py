from windylibraryproject import app, db

class Media (db.Model):
    madiaid = db.Column('MEDIA_ID', db.Integer, primary_key='true')
    status=db.Column ('STATUS', db.Integer)
    book = db.relationship('Book', backref="placement_hash")

    def __init__(self, mediaid,status):
        self.madiaid=mediaid
        self.status=status
