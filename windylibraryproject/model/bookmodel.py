from windylibraryproject import app, db
from windylibraryproject.model.mediamodel import Media

class Book(db.Model):
    isbn= db.Column('ISBN', db.String(20),primary_key='true')
    title= db.Column('TITLE', db.String(100))
    pubyear = db.Column('PUB_YEAR', db.DECIMAL)
    noofcopies = db.Column('NO_OF_COPIES', db.Integer)
    price = db.Column ('PRICE',db.Integer)
    media_id = db.Column('MEDIA_ID', db.ForeignKey(Media.madiaid))
    author = db.relationship('Author', backref="placement_hash")



    def __init__(self,isbn,title,pubyear,noofcopies,price,mediaid):
        self.isbn = isbn
        self.title=title
        self.pubyear = pubyear
        self.noofcopies = noofcopies
        self.price = price
        self.media_id = mediaid
