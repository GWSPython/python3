numberList = [1, 2, 3]
strList = ['one', 'two', 'three']

# Two iterables are passed ZIP two literators and made it as a
result = zip(numberList, strList)

resultSet = set(result)
print(resultSet)


numbersList = [8, 9, 10]
strList = ['eight', 'nine']
numbersTuple = ('EIGHT', 'NINE', 'TEN', 'FIFTEEN')

result = zip(numbersList, numbersTuple)

# Converting to set
resultSet = set(result)
print(resultSet)

result = zip(numbersList, strList, numbersTuple)

# Converting to set
resultSet = set(result)
print(resultSet)