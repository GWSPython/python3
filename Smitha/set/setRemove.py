name = {'Smitha', 'Gabe', 'Windy'}

# 'Smitha' element is removed
name.remove('Smitha')

# Updated name set
print('Updated name set: ', name)