def addIntoaSet(newName):
    name = {'Smitha', 'Gabe', 'Windy'}

    # 'Steve' element is added
    name.add(newName)

    # Updated name set
    print('Updated name set: ', name)

    return name

addIntoaSet('Steve')
