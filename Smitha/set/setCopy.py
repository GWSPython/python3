#two ways you can copy but it has different impact

name = {'Smitha', 'Gabe', 'Windy'} #copy using = operator same memory if you change one other will be affected
name1 = name
name1.add('Steve')
print(name1)
print(name)

name = {'Smitha', 'Gabe', 'Windy'} #copy using copy , original not affected
name1 = name.copy()
name1.add('Steve')
print(name1)
print(name)


