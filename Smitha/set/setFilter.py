nameSet = {'Smitha',"Gabe","Windy"}

def checkFemaleGender(name):

    if(name == 'Gabe'):
        return False
    if (name in 'Smitha' or 'Windy'):
        return True

results = filter(checkFemaleGender,nameSet)

for result in results:
    print(result)
