address = {"Gabe": "1234 GeorgeTown", "Windy": "8907 Leander", "Smitha": "8765 Austin"}

print("Address of Gabe " +address["Gabe"])
print("Address of Windy "+address["Windy"])

record = {'name': {'first': 'Stephen', 'last': 'Oates', 'middle': 'Gabe'},
          'job': ['developer', 'devops'],
          'age': 23}

print(record["name"])
print(record["name"]["last"])
print(record['job'])
print(record['job'].append('dba'))
print(record['job'])
print(record['age'])
# if we are not using this memory is cleared
# Technically speaking, Python has a feature known as garbage collection that
# cleans up unused memory as your program runs and frees you from
# having to manage such details in your code.

if not 'address' in record:
    print("missing")
if 'job' in record:
    print('not missing')

states = {"TX": "Texas", "CO": "Colorado", "NY": "New York"}

keylist = list(states.keys())
keylist.sort()
print(keylist)

for key in keylist:
    print(key,"===>",states[key])





