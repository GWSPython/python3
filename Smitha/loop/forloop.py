L = [200,100,2,3]
for num in L:
    print(num,end=',')

#break break the iterations

print("\n")
for num in L:
    if (num == 100):
        print("We found 100")
        break;
    print(num)

#continue - go to next iteration
print("\n")
for num in L:
    if (num == 100):
        print("We found 100")
        continue
    print(num)

    # loop within a loop inner loop

nums = [1, 2]

for num in nums:
    for letter in "abc":
        print(num, letter)

#range function

for i in range(10):
    print(i,end=',')

print("\n")
for i in range(1,10): #start postion and end poistion
    print(i, end=',')
