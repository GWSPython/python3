
alter table library.customer add column cust_phone varchar(50) null;
ALTER TABLE library.customer AUTO_INCREMENT = 5000;
ALTER TABLE library.customer modify cust_email varchar(50) null;
ALTER TABLE library.customer modify cust_phone varchar(50) null;
ALTER TABLE library.customer CHANGE addr_in addr_id int;