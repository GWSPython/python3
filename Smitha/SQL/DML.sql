
-- Populate Address table

insert into library.ADDRESS(addr_street,addr_city,addr_state,addr_zip)
        values('2087 Timberbrook Lane', 'Austin', 'TX', '78681');-- 1
insert into library.ADDRESS(addr_street,addr_city,addr_state,addr_zip)
        values('1465 Marion Drive', 'Austin', 'TX', '78652');   -- 2    
insert into library.ADDRESS(addr_street,addr_city,addr_state,addr_zip)
		values('974 Bingamon Branch Rd', 'Round Rock', 'TX', '78718');        
insert into library.ADDRESS(addr_street,addr_city,addr_state,addr_zip)
		values('3022 Lords Way', 'Round Rock', 'TX', '78717');        
insert into library.ADDRESS(addr_street,addr_city,addr_state,addr_zip)-- 5
		values('93 Sunny Glen Ln',  'Austin', 'TX', '78681');        
insert into library.ADDRESS(addr_street,addr_city,addr_state,addr_zip)
		values('4356 Pooh Bear Lane',  'Austin', 'TX', '78681');        
insert into library.ADDRESS(addr_street,addr_city,addr_state,addr_zip)
		values('3842 Willow Oaks Lane',  'Austin', 'TX', '78721');        
insert into library.ADDRESS(addr_street,addr_city,addr_state,addr_zip)
		values('1894 Wines Lane',  'Cedar park', 'TX', '78755');   -- 8     
insert into library.ADDRESS(addr_street,addr_city,addr_state,addr_zip)
		values('279 Raver Croft Drive',  'Cedar park', 'TX', '78755');-- 9
insert into library.ADDRESS(addr_street,addr_city,addr_state,addr_zip)
		values('1124 Broadcast Drive',  'Austin', 'TX', '78765');
commit;


-- Populate CustomerTable table


INSERT INTO library.Customer( cust_first_name, cust_last_name, cust_dob, cust_email,cust_in_zone,cust_phone,addr_id) VALUES
('Jason', 'Gray', '1990-09-08', 'jlgray@gmail.com',true,'970-273-9237',1);
INSERT INTO library.Customer( cust_first_name, cust_last_name, cust_dob, cust_email,cust_in_zone,cust_phone,addr_id) VALUES
('Mary', 'Prieto', '1999-10-18', 'mlprieto@yahoo.com',false,'813-487-4873',3);
INSERT INTO library.Customer( cust_first_name, cust_last_name, cust_dob, cust_email,cust_in_zone,cust_phone,addr_id) VALUES
('Roger', 'Hurst', '1996-12-05', 'rhurst@yahoo.com',true,'847-221-4986',2);
INSERT INTO library.Customer( cust_first_name, cust_last_name, cust_dob, cust_email,cust_in_zone,cust_phone,addr_id) VALUES
('Warren', 'Woodson', '1998-12-05', 'wvwoodson@ar.rrisd.com',false,'731-845-0077',4);
INSERT INTO library.Customer( cust_first_name, cust_last_name, cust_dob, cust_email,cust_in_zone,cust_phone,addr_id) VALUES
('Steven', 'Jensen', '1999-10-05', 'sjensen@ar.rrisd.com',true,'216-789-6442',5);
INSERT INTO library.Customer( cust_first_name, cust_last_name, cust_dob, cust_email,cust_in_zone,cust_phone,addr_id) VALUES
('Dav', 'Bain', '1994-10-23', 'dbain@gmail.com',true,'864-610-9558',6);
INSERT INTO library.Customer( cust_first_name, cust_last_name, cust_dob, cust_email,cust_in_zone,cust_phone,addr_id) VALUES
('Ruth', 'Alber', '1988-06-25', 'rpalber@gmail.com',true,'337-316-3161',7);
INSERT INTO library.Customer( cust_first_name, cust_last_name, cust_dob, cust_email,cust_in_zone,cust_phone,addr_id) VALUES
('Sally', 'Schilling', '1978-06-25', 'sjschilling@gmail.com',false,'832-366-9035',8);
INSERT INTO library.Customer( cust_first_name, cust_last_name, cust_dob, cust_email,cust_in_zone,cust_phone,addr_id) VALUES
('John', 'Byler', '1986-06-29', 'jmbyler@gmail.com',false,'423-592-8630',9);
INSERT INTO library.Customer( cust_first_name, cust_last_name, cust_dob, cust_email,cust_in_zone,cust_phone,addr_id) VALUES
('Kevin', 'Spruell', '1986-02-13', 'kSpruell@gmail.com',true,'223-509-8930',10);



INSERT INTO library.Lib_card(card_num, fine,membership_fee,expiry_date,issue_date,cust_id) VALUES ( 5767052, 0.0,0.0,'2020-02-13','2019-02-13', 5000);
INSERT INTO library.Lib_card(card_num, fine,membership_fee,expiry_date,issue_date,cust_id) VALUES ( 5532681, 0.0,60.0,'2019-08-23','2019-06-23', 5001);
INSERT INTO library.Lib_card(card_num, fine,membership_fee,expiry_date,issue_date,cust_id) VALUES ( 2197620, 0.0,0.0,'2020-05-11','2019-05-11', 5002);
INSERT INTO library.Lib_card(card_num, fine,membership_fee,expiry_date,issue_date,cust_id) VALUES ( 9780749, 0.0,60.0,'2019-03-29','2019-01-29', 5003);
INSERT INTO library.Lib_card(card_num, fine,membership_fee,expiry_date,issue_date,cust_id) VALUES ( 1521412, 0.0,0.0,'2020-02-10','2019-02-10', 5004);
INSERT INTO library.Lib_card(card_num, fine,membership_fee,expiry_date,issue_date,cust_id) VALUES ( 3920486, 0.0,0.0,'2020-03-15','2019-03-15', 5005);
INSERT INTO library.Lib_card(card_num, fine,membership_fee,expiry_date,issue_date,cust_id) VALUES ( 2323953, 0.0,0.0,'2020-09-10','2019-09-10', 5006);
INSERT INTO library.Lib_card(card_num, fine,membership_fee,expiry_date,issue_date,cust_id) VALUES ( 4387969, 0.0,60.0,'2019-08-23','2019-06-23', 5007);
INSERT INTO library.Lib_card(card_num, fine,membership_fee,expiry_date,issue_date,cust_id) VALUES ( 4444172, 0.0,60.0,'2019-06-20','2019-04-20', 5008);
INSERT INTO library.Lib_card(card_num, fine,membership_fee,expiry_date,issue_date,cust_id) VALUES ( 2645634, 0.0,0.0,'2020-09-22','2019-09-22', 5009);

#library branch address
insert into library.ADDRESS(addr_street,addr_city,addr_state,addr_zip)
		values('4832 Deercove Drive',  'Austin', 'TX', '78681');
insert into library.ADDRESS(addr_street,addr_city,addr_state,addr_zip)
		values('2888 Oak Avenue',  'Austin', 'TX', '78717');
insert into library.ADDRESS(addr_street,addr_city,addr_state,addr_zip)
		values('2063 Washburn Street',  'Austin', 'TX', '78234');
        
        
 INSERT INTO library.Lib_details (lib_branch_id, lib_name,lib_phone,addr_id) values
 (2000,"Millwood branch",'512-908-8765',11);
  INSERT INTO library.Lib_details (lib_branch_id, lib_name,lib_phone,addr_id) values
 (2500,"Spicewood branch",'512-908-8765',12);
 INSERT INTO library.Lib_details (lib_branch_id, lib_name,lib_phone,addr_id) values
 (2800,"Little Walnut branch",'512-908-8765',13);
 
INSERT INTO library.login (username,password,cust_id) VALUES('jlgray','password1',5000);
INSERT INTO library.login (username,password,cust_id) VALUES('mlprieto','password2',5001);
INSERT INTO library.login (username,password,cust_id) VALUES('rhurst','password3',5002);
INSERT INTO library.login (username,password,cust_id) VALUES('wvwoodson','password4',5003);
INSERT INTO library.login (username,password,cust_id) VALUES('sjensen','password5',5004);
INSERT INTO library.login (username,password,cust_id) VALUES('dbain','password6',5005);
INSERT INTO library.login (username,password,cust_id) VALUES('rpalber','password7',5006);
INSERT INTO library.login (username,password,cust_id) VALUES('sjschilling','password8',5007);
INSERT INTO library.login (username,password,cust_id) VALUES('jmbyler','password9',5008);
INSERT INTO library.login (username,password,cust_id) VALUES('kSpruell','password9',5009);

INSERT INTO LIBRARY.STATUS(STATUS_CODE, STATUS_DESC) VALUES (1, 'Available');
INSERT INTO LIBRARY.STATUS(STATUS_CODE, STATUS_DESC) VALUES (2, 'In Transit');
INSERT INTO LIBRARY.STATUS(STATUS_CODE, STATUS_DESC) VALUES (3, 'Checked Out');
INSERT INTO LIBRARY.STATUS(STATUS_CODE, STATUS_DESC) VALUES (4, 'On Hold');


INSERT INTO LIBRARY.MEDIA(MEDIA_ID,STATUS) VALUES (2000, 1);
INSERT INTO LIBRARY.BOOK(ISBN,TITLE,PUB_YEAR,PRICE,NO_OF_COPIES,MEDIA_ID) VALUES ('978-0743289412', 'Lisey''s Story',2006,10.0,20,2000);
INSERT INTO LIBRARY.AUTHOR VALUES(1000,'Stephen','King','978-0743289412');


INSERT INTO LIBRARY.MEDIA(MEDIA_ID,STATUS) VALUES (2001, 1);
INSERT INTO LIBRARY.BOOK(ISBN,TITLE,PUB_YEAR,PRICE,NO_OF_COPIES,MEDIA_ID) VALUES ('978-1596912366', 'Restless: A Novel',2009,100.0,20,2001);
INSERT INTO LIBRARY.AUTHOR VALUES(1001,'William','Boyd','978-1596912366');

INSERT INTO LIBRARY.MEDIA(MEDIA_ID,STATUS) VALUES (2002, 1);
INSERT INTO LIBRARY.BOOK(ISBN,TITLE,PUB_YEAR,PRICE,NO_OF_COPIES,MEDIA_ID) VALUES ('978-0312351588', 'Beachglass',2019,80.0,10,2002);
INSERT INTO LIBRARY.AUTHOR VALUES(1002,'Wendy','Blackburn','978-0312351588');

INSERT INTO LIBRARY.MEDIA(MEDIA_ID,STATUS) VALUES (2003, 1);
INSERT INTO LIBRARY.BOOK(ISBN,TITLE,PUB_YEAR,PRICE,NO_OF_COPIES,MEDIA_ID) VALUES ('978-0156031561', 'The Places In Between',2012,180.0,10,2003);
INSERT INTO LIBRARY.AUTHOR VALUES(1003,'Rory','Stewart','978-0156031561');


INSERT INTO LIBRARY.MEDIA(MEDIA_ID,STATUS) VALUES (2004, 1);
INSERT INTO LIBRARY.BOOK(ISBN,TITLE,PUB_YEAR,PRICE,NO_OF_COPIES,MEDIA_ID) VALUES ('978-0060583002', 'The Last Season',2001,50.0,3,2004);
INSERT INTO LIBRARY.AUTHOR VALUES(1004,'Eric','Blehm','978-0060583002');

INSERT INTO LIBRARY.MEDIA(MEDIA_ID,STATUS) VALUES (2005, 1);
INSERT INTO LIBRARY.BOOK(ISBN,TITLE,PUB_YEAR,PRICE,NO_OF_COPIES,MEDIA_ID) VALUES ('978-0316740401', 'Case Histories: A Novel',2005,50.0,1,2005);
INSERT INTO LIBRARY.AUTHOR VALUES(1005,'Kate','Atkinson','978-0316740401');

INSERT INTO LIBRARY.MEDIA(MEDIA_ID,STATUS) VALUES (2006, 1);
INSERT INTO LIBRARY.BOOK(ISBN,TITLE,PUB_YEAR,PRICE,NO_OF_COPIES,MEDIA_ID) VALUES ('978-0316013949', 'Step on a Crack',2004,50.0,4,2006);
INSERT INTO LIBRARY.AUTHOR VALUES(1006,'James','Patterson','978-0316013949');

INSERT INTO LIBRARY.MEDIA(MEDIA_ID,STATUS) VALUES (2007, 1);
INSERT INTO LIBRARY.BOOK(ISBN,TITLE,PUB_YEAR,PRICE,NO_OF_COPIES,MEDIA_ID) VALUES ('978-0374105235', 'Long Way Gone: Memoirs of a Boy Soldier',2000,50.0,1,2007);
INSERT INTO LIBRARY.AUTHOR VALUES(1007,'Beah','Patterson','978-0374105235');


INSERT INTO LIBRARY.MEDIA(MEDIA_ID,STATUS) VALUES (2008, 1);
INSERT INTO LIBRARY.BOOK(ISBN,TITLE,PUB_YEAR,PRICE,NO_OF_COPIES,MEDIA_ID) VALUES ('978-0385340229', 'Sisters',1999,150.0,1,2008);
INSERT INTO LIBRARY.AUTHOR VALUES(1008,'Danielle','Steel','978-0385340229');

INSERT INTO LIBRARY.MEDIA(MEDIA_ID,STATUS) VALUES (2009, 1);
INSERT INTO LIBRARY.VIDEO(VIDEO_ID,TITLE,RELEASE_YEAR,PRICE,NO_OF_COPIES,MEDIA_ID,RATING,DIRECTOR_FNAME,DIRECTOR_LNAME) VALUES 
(3000, 'Terminator 2',1991,10.0,20,2009,'PG13','James','Cameron');

INSERT INTO LIBRARY.MEDIA(MEDIA_ID,STATUS) VALUES (2010, 1);
INSERT INTO LIBRARY.VIDEO(VIDEO_ID,TITLE,RELEASE_YEAR,PRICE,NO_OF_COPIES,MEDIA_ID,RATING,DIRECTOR_FNAME,DIRECTOR_LNAME) VALUES 
(3001, 'Raiders of the Lost Ark',1981,20.0,5,2010,'PG13','Steven','Spielberg');

INSERT INTO LIBRARY.MEDIA(MEDIA_ID,STATUS) VALUES (2011, 1);
INSERT INTO LIBRARY.VIDEO(VIDEO_ID,TITLE,RELEASE_YEAR,PRICE,NO_OF_COPIES,MEDIA_ID,RATING,DIRECTOR_FNAME,DIRECTOR_LNAME) VALUES 
(3002, 'Aliens',1986,20.0,5,2011,'PG13','James','Cameron');

INSERT INTO LIBRARY.MEDIA(MEDIA_ID,STATUS) VALUES (2012, 1);
INSERT INTO LIBRARY.VIDEO(VIDEO_ID,TITLE,RELEASE_YEAR,PRICE,NO_OF_COPIES,MEDIA_ID,RATING,DIRECTOR_FNAME,DIRECTOR_LNAME) VALUES 
(3003, 'Die Hard',1988,20.0,5,2012,'PG13','John','McTiernan');


INSERT INTO LIBRARY.MEDIA(MEDIA_ID,STATUS) VALUES (2013, 1);
INSERT INTO LIBRARY.VIDEO(VIDEO_ID,TITLE,RELEASE_YEAR,PRICE,NO_OF_COPIES,MEDIA_ID,RATING,DIRECTOR_FNAME,DIRECTOR_LNAME) VALUES 
(3004, 'Die Hard',1988,20.0,5,2013,'PG13','John','McTiernan');


INSERT INTO LIBRARY.MEDIA_LIB (lIB_id,MEDIA_ID)VALUES (2000,2000);
INSERT INTO LIBRARY.MEDIA_LIB(lIB_id,MEDIA_ID) VALUES (2500,2000);
INSERT INTO LIBRARY.MEDIA_LIB(lIB_id,MEDIA_ID) VALUES (2500,2001);
INSERT INTO LIBRARY.MEDIA_LIB(lIB_id,MEDIA_ID) VALUES (2000,2002);
INSERT INTO LIBRARY.MEDIA_LIB (lIB_id,MEDIA_ID)VALUES (2500,2003);
INSERT INTO LIBRARY.MEDIA_LIB(lIB_id,MEDIA_ID) VALUES (2500,2004);
INSERT INTO LIBRARY.MEDIA_LIB (lIB_id,MEDIA_ID)VALUES (2800,2005);
INSERT INTO LIBRARY.MEDIA_LIB(lIB_id,MEDIA_ID) VALUES (2800,2006);
INSERT INTO LIBRARY.MEDIA_LIB(lIB_id,MEDIA_ID) VALUES (2500,2007);
INSERT INTO LIBRARY.MEDIA_LIB(lIB_id,MEDIA_ID) VALUES (2000,2008);
INSERT INTO LIBRARY.MEDIA_LIB(lIB_id,MEDIA_ID) VALUES (2500,2008);
INSERT INTO LIBRARY.MEDIA_LIB(lIB_id,MEDIA_ID) VALUES (2500,2009);
INSERT INTO LIBRARY.MEDIA_LIB (lIB_id,MEDIA_ID)VALUES (2500,2010);
INSERT INTO LIBRARY.MEDIA_LIB(lIB_id,MEDIA_ID) VALUES (2500,2011);
INSERT INTO LIBRARY.MEDIA_LIB(lIB_id,MEDIA_ID) VALUES (2500,2011);
INSERT INTO LIBRARY.MEDIA_LIB(lIB_id,MEDIA_ID) VALUES (2000,2012);


INSERT INTO LIBRARY.CHECKOUT(MEDIA_ID,CARD_NUM,CHECKOUT_DATE,DUE_DATE,RETURNED_FLAG)
 VALUES (2012,4444172,'2019-06-01','2019-06-30',FALSE);
 
 INSERT INTO LIBRARY.CHECKOUT(MEDIA_ID,CARD_NUM,CHECKOUT_DATE,DUE_DATE,RETURNED_FLAG)
 VALUES (2000,2645634,'2019-05-01','2019-05-30',TRUE);
 
  INSERT INTO LIBRARY.CHECKOUT(MEDIA_ID,CARD_NUM,CHECKOUT_DATE,DUE_DATE,RETURNED_FLAG)
 VALUES (2005,1521412,'2019-03-01','2019-03-30',FALSE);
 

