class MemoryReferencence:

    def sharedMemroyAllocation(self_self):
        L = [1, 2, 3]
        L1 = L
        L = [24]
        for item in L:
            print(item)
        for item1 in L1:
            print(item1)

    pass

    def sharedMemroy(self_self):
        L = [122, 222, 334]
        L1 = L
        for item in L:
            print(item, end=',')
        for item1 in L1:
            print(item1, end=',')

        print(id(L1))
        print(id(L))

    pass

    def isEqual(self):
        L1 = [1, 2, 3]
        L2 = [1, 2, 3]
        print(L1==L2)#same values
        print(L1 is L2) #differen objects - different memory location
        print(id(L1))
        print(id(L2))

        name ="windy"
        name1 = "windy"
        print(name1 == name)  # same values
        print(name1 is name)  # differen objects - different memory location
        #but small integers and strings are cached python says its same refernce

    pass


    def testSkill(self_self):
        A = "texas"
        B = A;
        B = "california"
        print(A)

        C = ["Austin"]
        D = C
        D[0]="Dallas"
        print(C)

        E = ["Houston"]
        F = E[:]
        print(F)
        F[0]=["Waco"]
        print(E)
    pass


mr = MemoryReferencence()
mr.sharedMemroyAllocation()
mr.sharedMemroy()
mr.isEqual()
mr.testSkill()
