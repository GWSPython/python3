import re
#split function mention the delimiter
name = "smitha*gabe*windy"
print(name)
name = name.split("*")
print(name)


#replace functionr
name = "smitha"
print(name)
name = name.replace("tha",'ta')
print(name)

#find functionr
name = "smitha"
print(name.find("mi"))


#search from end of the string
name = "smitha"
print(name[-2])

#new line \n
#tab \t
name = "smitha"+"\n"+"Gabe"+"\t"+"windy"
print(name)

#rstrip - strip the new line
name = "smitha"+"Gabe\n"
print(name+"*****")

name = name.rstrip()#strips the space so **** do not go to next line
print(name+"****")

#each letter has specific position W ->0i->1 n->2 d->3 y->4
name = "Windy"
for c in name:
    print(c)

name = "rohan" #uppercase conversion
print(name.upper())

x = 4
while(x > 0): # as long as x is greater than 0 this will run, when it becomes 0 this loop will break
    print("smitha"*x)
    print(x)
    x = x -1 # subtract each time one from x

#pattern matching
#starts with Python then followed by 0 or more tabs or space and then ends with is tough.if such substring exists then find that
match = re.match('Python[ \t]*(.*) is tough','Python     language is tough')
print(match.groups())