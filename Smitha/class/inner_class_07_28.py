class Student: #outer class

    def __init__(self, rollno, name):
        self.name = name
        self.rollno = rollno
        self.laptop = self.Laptop()

    def show(self):
            print(self.name,self.rollno,self.laptop.brand,self.laptop.os)


    class Laptop: #inner class

        def __init__(self):
            self.brand = "HP"
            self.os = "windows"


student = Student(1,'smitha')
student.show()

laptop = Student(2,'windy').Laptop()
print( laptop.brand, laptop.os)