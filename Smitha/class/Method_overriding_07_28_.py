class Cat:

    def sound(self):
        print ('Meow')

    def walk(self):
        print("walk")

    def eat(self):
        print("eat")

class Tiger(Cat):

    def sound(self):
        print('roar')

    def walk(self):
        super().walk()
        print("run very fast")


if __name__ == '__main__':
    t = Tiger()
    t.sound()
    t.walk()
    t.eat()