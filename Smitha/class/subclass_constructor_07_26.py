class Book:
    def __init__(self, ISBN, type, name, author, price=10):
        self.isbn = ISBN
        self.bookName = name
        self.bookAuthor = author
        self.price = price
        self.booktype = type

    def getBookName(self):
        return self.bookName

    def getType(self):
        return self.booktype



class ChildrenBook(Book):
    def __init__(self, ISBN, name, author):  # Redefine constructor
       Book.__init__(self, ISBN, "Children's book", name, author)    # Run original with 'book'
      # super().__init__(self, ISBN, "Children's book", name, author)



if __name__ == '__main__':
    book1 = Book('1234','regular' 'Lord of the rings', 'John Smith', 100)
    book3 = ChildrenBook('9087', 'Elmo the monster', 'Gabe Oates') #inherit parent

    print(book3.getType())
