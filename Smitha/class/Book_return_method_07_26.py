class Book:
    def __init__(self, ISBN, name, author,price=10):
        self.isbn = ISBN
        self.bookName = name
        self.bookAuthor = author
        self.price = price

    def getBookName(self):
        return self.bookName

    def getLastName(self):
        return self.bookAuthor.split()[1]

    def lateFee(self):
        self.lateFee = 0
        if(self.price < 50):
             self.lateFee = 1
        elif(self.price > 50 and self.price < 101):
             self.lateFee = 4
        return self.lateFee


if __name__ == '__main__':
    book1 = Book('1234', 'Lord of the rings','John Smith',100)
    print(book1.bookName)
    print(book1.getLastName())
    print(book1.lateFee())
    book2 = Book('8907', 'Lord of the rings2', 'John Smith')
    print(book2.bookName)
    print(book2.getLastName())
    print(book2.lateFee())

