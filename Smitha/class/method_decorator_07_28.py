def div(a,b):
    return a/b

def modify_div(func): #take function as programmer
    def inner(a,b):
        if a< b:
         a,b = b,a
        return func(a,b)
    return inner

div1 = modify_div(div)
print(div1(2,4))