# function call itself

def fact(num):
    if (num == 0):
       return 1
    factorial = num * fact(num - 1)
    return factorial

if __name__ == '__main__':
    print(fact(5))
