# prepare
# recipe
# cleanup
from abc import ABC, abstractmethod  # template method design method


class AbstractRecipe(ABC):
    def execute(self):
        self.prepare()
        self.reciepe()
        self.cleanup()

    @abstractmethod
    def prepare(self): pass

    @abstractmethod
    def reciepe(self): pass

    @abstractmethod
    def cleanup(self): pass


class Cake(AbstractRecipe):

    def prepare(self):
        print("get flour,egg,milk,aking powder,start oven")

    def reciepe(self):
        print("mix ingredients and put in oven")

    def cleanup(self):
        pass


Cake().execute()


class RedCurry(AbstractRecipe):

    def prepare(self):
        print("get coconut milk,chilli powder, palm sugar, vessels")

    def reciepe(self):
        print("mix ingredients and put in stove")

    def cleanup(self):
        pass


RedCurry().execute()