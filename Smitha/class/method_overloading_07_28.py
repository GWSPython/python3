class A:

    def add(self, x, b, c):
        return x + b + c
    
    def add(self, x, b):
        return x + b



a1 = A()
print(a1.add(2,4))
print(a1.add(2,4,5))

#method overloading is not possible in pythong