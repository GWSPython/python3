from abc import ABC, abstractmethod


# add anotation and extend a class Abstract Base class
class AbstractAnimal(ABC):
    # abstract I don't know how the animal ark
    @abstractmethod  # decorator called abstract
    def bark(self): pass




# print(AbstractAnimal())

class Doberman(AbstractAnimal):

    def bark(self):
        print("ruf ruf ruf")


class Lab(AbstractAnimal):

    def bark(self):
        print("ruf ")


print(Doberman().bark())
print(Lab().bark())
