class Book: #inherit the behavior behavior and property of the parent
    #All classes inherit from object
    def __init__(self, ISBN, name, author, price=10):
        self.isbn = ISBN
        self.bookName = name
        self.bookAuthor = author
        self.price = price

    def getBookName(self):
        return self.bookName

    def getLastName(self):
        return self.bookAuthor.split()[1]

    def lateFee(self):
        lateFee = 0
        if(self.price < 50):
            lateFee = 1
        elif(self.price > 50 and self.price < 101):
             lateFee = 4
        return lateFee

    def lostFees(self, fineCharge = 2 ):
        return self.price * fineCharge;





class ChildrenBook(Book):
    def lateFee(self):
        self.lateFee = 0
        if (self.price < 50):
            self.lateFee = 3
        elif (self.price > 50 and self.price < 101):
            self.lateFee = 10
        return self.lateFee

    def lostFees(self):
        return Book.lostFees(self,4);



if __name__ == '__main__':
    book1 = Book('1234', 'Lord of the rings', 'John Smith', 100)
    book3 = ChildrenBook('9087', 'Elmo the monster', 'Gabe Oates') #inherit parent
    print(book3.getLastName())
    print(book3.lostFees())
