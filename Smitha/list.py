# List is positionly ordered collection
# [2,3,4]

# list of employees in a company
EmployeeList = ['Gabe', 'Windy', 'Smitha']
for emp in EmployeeList:
    print(emp)


for emp in EmployeeList[:-1]:
    print(emp)


EmployeeList.append("Cece")
for emp in EmployeeList:
    print(emp, end=',')

EmployeeList = EmployeeList + ['Steve', "Christy", "Manoj", "Glen"]
print()
EmployeeList.sort()
for emp in EmployeeList:
    print(emp, end=',')


print()
EmployeeList.sort(reverse=True)
for emp in EmployeeList:
    print(emp, end=',')

EmployeeList = EmployeeList * 2
print()
print(EmployeeList.__len__())
for emp in EmployeeList:
    print(emp,end=',')


phoneNumberList = ["512345678", "4567890987"]
