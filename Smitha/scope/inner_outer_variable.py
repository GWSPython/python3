# def _outer():
#     x = 'outer x'
#     def inner():
#         x = 'inner x'
#         print(x)
#
#     inner()
#     print(x)
# _outer()
#
# '''
# LEGB
# '''

x = 'smitha'

def _outerNonLocal():
    x = 'outer x'
    def inner():
        nonlocal x
        x = 'inner x'
        print("**************")
        print(x)

    inner()
    print("&&&&&&&&&&&&&&")
    print(x)

_outerNonLocal()
print(x)