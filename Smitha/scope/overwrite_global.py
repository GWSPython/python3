
x = 'Global x'

def _test():
    y = 'local y'
    x = y
    print(x)
    print(y)

def _test2():
    global x
    y = 'local y'
    x = 'local x'
    print(x)
    print(y)



_test()
print(x)
_test2()
print(x)