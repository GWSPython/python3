from flask import Flask, render_template, url_for, flash, redirect, request
from forms import LoginForm
from alchemy import Login
from flask_sqlalchemy import SQLAlchemy
app = Flask(__name__)
app.config['SECRET_KEY'] = '5791628bb0b13ce0c676dfde280ba245'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:Baba123$$$India@localhost:3306/library'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

app.config['SECRET_KEY'] = '5791628bb0b13ce0c676dfde280ba245'





@app.route("/home", methods=['GET', 'POST'])
def home():
    return render_template('home.html')


@app.route("/contact", methods=['GET', 'POST'])
def contact():
    return render_template('contact.html')


@app.route("/search", methods=['GET', 'POST'])
def search():
    return render_template('search.html')


@app.route("/register", methods=['GET', 'POST'])
def register():
    return render_template('registration.html')

@app.route("/recentnews", methods=['GET', 'POST'])
def recentnews():
    return render_template('recentnews.html')


@app.route("/upcoming", methods=['GET', 'POST'])
def upcoming():
    return render_template('upcomingevents.html')


@app.route("/calendar", methods=['GET', 'POST'])
def calendar():
    return render_template('calendar.html')


@app.route("/login2", methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        return "username is" + request.values["username"]
    else:
        return render_template('wsglogin.html')

@app.route("/login", methods=['GET', 'POST'])
def login1():
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate_on_submit():
        login = Login.query.filter_by(username=form.username.data,password=form.password.data).first()
        if login:
                return "username is" + form.username.data
        else:
           # flash('Login Unsuccessful. Please check email and password', 'danger')
           return "Login failed"
    else:
        return render_template('login.html', title="Login", form=form)

if __name__ == '__main__':
    app.run(debug=True)
