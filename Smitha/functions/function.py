# some instructions pckaged together
# def keyword definition
# resue code without repetition +DRY

def hello_func():
    print("Hello function!")


print(hello_func)
# hello_func()

# instead of changing in 4 place, just change in single location
print("Hello function!")
print("Hello function!")
print("Hello function!")
print("Hello function!")


# return
def hi_function():
    return "Hello function!!!"


print(hi_function())


def greet_fucntion(Greeting, name):
    return Greeting + " " + name


print(greet_fucntion("Good Morning", "smitha"))
print(greet_fucntion("Good Evening", "windy"))
print(greet_fucntion("Good Afternoon", "Gabe"))


def greet_fucntion1(Greeting, name='You'):
    return '{},{}'.format(Greeting, name)


print(greet_fucntion1("Good Morning"))
print(greet_fucntion1("Good Evening", "windy"))
print(greet_fucntion1("Good Afternoon", "Gabe"))

def student(*args,**kwargs):
    print(args)#tuple
    print(kwargs)#dict

student("Math","Statistics",name="Gabe Oates",major="Mathematics",minor="statistics")

sub = ["Math","Statistics"]
student1 = {'name':"Gabe Oates",'major':"Mathematics",'minor':"statistics"}
student(sub,student1)

student(*sub,**student1)