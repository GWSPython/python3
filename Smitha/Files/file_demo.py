#file objects

def readFile():
    f = open('test.txt','r')
    print(f.name)
    f.close() #mandatory to close


def readFileReadMethod(): #contextmanager
   with open('test.txt','r') as f:
       f_contents = f.read()
       print(f_contents)
   f.close()

def readFileReadWithCharSizeMethod(): #contextmanager
   with open('test.txt','r') as f:
       f_contents = f.read(100)
       print(f_contents)
       print("*************")

       f_contents = f.read(100)
       print(f_contents)

def readLines(): #contextmanager
   with open('test.txt','r') as f:
       f_contents = f.readlines()
       print(f_contents)

def readLine(): #contextmanager
   with open('test.txt','r') as f:
       f_contents = f.readline()
       print(f_contents)


def readLineThroughLoop():  # no memory issue
    with open('test.txt', 'r') as f:
        for line in f:
            print(line)


#read large file

with open('test.txt', 'r') as f:
    sizetoread = 100

#readFile()
#readFileReadMethod()
#readFileReadWithCharSizeMethod()
readLines()
#readLine()
readLineThroughLoop()