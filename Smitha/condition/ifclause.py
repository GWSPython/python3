


if True: #if condition is true
    print("it is true")

if False:
    print("it is false")





#Equal ==
#Not Equal !=
#Greater than >
#less than <
#Greater or equal >=
#Less or equal <=
#Object identity is

country ="US"
if(country == "US"): #== checking equality -> if true
    print("country is united States")
else:
    print("country is not united states")

country1 ="Mexico"
if(country1 == "US"): #== checking equality -> if true
    print("continent  is North America")
elif (country1 == "Mexico"):
    print("continent is North america")
else:
    print("continent is Asia")


#Or operator
if(country1 == 'US' or country1 == "Mexico"):
    print("continent  is North America")
else  :
    print("continent is Asia")

#And operator
user = 'Admin'
logged_in = True

if(user == 'Admin' and logged_in):
    print('Valid User')

logged_in = False
if(user == 'Admin' and not logged_in):
    print('Please login')

user1 = user

if (user1 is user):
    print("same object")


x = 100
y = 80
z = 45

p = 110
q = 45

if x > y:
    print ("x is greater than y")

if(z < x):
     print ("z is less than x")

if( y < x and z < x):
    print ("x is greter than both y and z")

if( z < y or p < y):
    print("one of the value is less than y")

if (z <= q):
    print("z is less than or equa to q")

if (z != y):
    print("z and y are not equal")


#FALSE conditions

#False
#None
#zero of any numeric
#any empty sequence'',(),{}
#any empty mapping {}

condition = False

if  condition:
    print("Evaluated to True")
else:
    print("Evaluated to false")

condition = None

if  condition:
    print("Evaluated to True")
else:
    print("Evaluated to false")

condition = [] #empty list

if condition:
    print("Evaluated to True")
else:
    print("Evaluated to false")

condition = {} #empty tuple

if condition:
    print("Evaluated to True")
else:
    print("Evaluated to false")

condition = ''
if condition:
    print("Evaluated to True")
else:
    print("Evaluated to false")



condition = 0
if  condition:
    print("Evaluated to True")
else:
    print("Evaluated to false")




