from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, DateField, SelectField
from wtforms.validators import DataRequired, Length


class RegisterForm(FlaskForm):
    # areacode = SelectField(
    #     choices=[('512', '+512'), ('+210', '210')])
    firstname = StringField("Firstname", validators=[DataRequired])
    lastname = StringField("Lastname", validators=[DataRequired])
    dateofbirth = DateField("DOB", validators=[DataRequired])
    email = StringField("Email", validators=[DataRequired])
    address1 = StringField("Address", validators=[DataRequired])
    address2 = StringField("Address2")
    city = StringField("City", validators=[DataRequired])
    state = StringField("State", validators=[DataRequired])
    phone = StringField("Phone", validators=[DataRequired])
    password1 = PasswordField("Password", validators=[DataRequired])
    password2 = PasswordField("Password2", validators=[DataRequired])
