from flask import Flask, render_template, url_for, flash, redirect, request, session
from smithalibraryproject import app, db
from smithalibraryproject.model.loginmodel import Login
from smithalibraryproject.model.customermodel import Customer
from smithalibraryproject.routes.loginform import LoginForm
from smithalibraryproject.routes.registerForm import RegisterForm
from smithalibraryproject.model.addressmodel import Address
from smithalibraryproject.model.bookmodel import Book
from smithalibraryproject.model.authormodel import Author
from smithalibraryproject.routes.searchForm import SearchForm
from smithalibraryproject.routes.searchresult import SearchResult
from sqlalchemy.sql import text

app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

@app.route("/home", methods=['GET', 'POST'])
def home():
    # if (request.args.get("username") or session.__getattr__("logginUser")):
    if 'logginUser' in session:
        username = session['logginUser']
        data = "Hi welcome " + username;
        return render_template('home.html', value=data)
    else:
        return render_template('home.html')


@app.route("/contact", methods=['GET', 'POST'])
def contact():
    return render_template('contact.html')


@app.route("/search", methods=['GET', 'POST'])
def search():
    SearchFrom = SearchForm(request.form)

    if request.method == 'POST':
        query = db.session.query(Book.title, Book.noofcopies, Author.authorFname, Author.authorLname). \
            join(Author, Author.ISBN == Book.ISBN) \
            .filter(Author.authorFname.like("%" + SearchFrom.authorSearch.data + "%"),
                    Book.title.like("%" + SearchFrom.bookSearch.data + "%"))
        searchresult = query.all()

        return render_template('search.html', len=len(searchresult), searchresult=searchresult)

    return render_template('search.html', form=SearchFrom)


@app.route("/register", methods=['GET', 'POST'])
def register():
    registerform = RegisterForm(request.form)
    if request.method == 'POST' and registerform.validate_on_submit():
        address = Address(registerform.address1.data, registerform.city.data, registerform.state.data, '89776')
        customer = Customer(custfirstname=registerform.firstname.data,
                            custlastname=registerform.lastname.data,
                            custdob=registerform.dateofbirth.data,
                            custemail=registerform.email.data,
                            custInZone=1,
                            custPhone=registerform.phone.data)

        login = Login(
            username=registerform.firstname.data[0].lower() + registerform.lastname.data.lower(),
            password=registerform.password1.data)
        session["logginUser"] = registerform.firstname.data[0].lower() + registerform.lastname.data.lower()
        customer.login.append(login)
        address.customer.append(customer)
        db.session.add(address)
        db.session.commit();

        return home() 
    return render_template('registration.html', form=registerform)


@app.route("/recentnews", methods=['GET', 'POST'])
def recentnews():
    return render_template('recentnews.html')


@app.route("/upcoming", methods=['GET', 'POST'])
def upcoming():
    return render_template('upcomingevents.html')


@app.route("/calendar", methods=['GET', 'POST'])
def calendar():
    return render_template('calendar.html')


@app.route("/login2", methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        return "username is" + request.values["username"]
    else:
        return render_template('wsglogin.html')


@app.route("/login", methods=['GET', 'POST'])
def login1():
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate_on_submit():
        if __name__ == '__main__':
            login = Login(username=form.username.data, password=form.password.data);
            result = db.session.query(Login, Customer).join(Customer, Login.cust_id == Customer.cust_id) \
                .filter(Login.username == login.username, Login.password == login.password).first()

            # flash('Login Unsuccessful. Please check email and password', 'danger')
            if result:
                session["logginUser"] = result.Customer.custfirstname + " " + result.Customer.custlastname
                return home()
            else:
                # flash('Login Unsuccessful. Please check email and password', 'danger')
                session.pop("logginUser")
                return "Login failed"
            return "Login failed"
    else:
        return render_template('wsglogin.html', title="Login", form=form)


if __name__ == '__main__':
    app.run(debug=True)
