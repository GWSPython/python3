from smithalibraryproject import app, db
from smithalibraryproject.model.bookmodel import Book


class Author(db.Model):
    author_id = db.Column('author_id', db.Integer, primary_key="true")
    authorFname = db.Column('Author_First_Name', db.String(50))
    authorLname = db.Column('Author_Last_Name', db.String(50))
    ISBN = db.Column(db.String(20), db.ForeignKey(Book.ISBN))

    def __init__(self, ISBN, authorFname, authorLname):
        self.ISBN = ISBN
        self.authorFname = authorFname
        self.authorLname = authorLname

    def __repr__(self):
        return '<User smitha is %r>' % self.ISBN
