from smithalibraryproject import app, db


class Book(db.Model):
    ISBN = db.Column('ISBN', db.String(20), primary_key='true')
    title = db.Column('Title', db.String(100))
    noofcopies = db.Column('NO_OF_COPIES', db.Integer)
    author = db.relationship("Author", backref="placement_hash")

    def __init__(self, title):
        self.title = title

    def __init__(self, ISBN, title, noofcopies):
        self.ISBN = ISBN
        self.title = title
        self.noofcopies = noofcopies

    def __repr__(self):
        return '<Isbn is %r>' % self.ISBN
