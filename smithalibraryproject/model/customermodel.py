from smithalibraryproject import app, db
from smithalibraryproject.model.addressmodel import Address
from sqlalchemy.dialects.mysql import MEDIUMINT, TINYINT
from sqlalchemy import create_engine
from sqlalchemy import MetaData



class Customer(db.Model):
    cust_id = db.Column('cust_id', db.Integer, primary_key='true')
    custfirstname = db.Column('cust_first_name', db.String(50))
    custlastname = db.Column('cust_last_name', db.String(10))
    custdob = db.Column('cust_dob', db.Date)
    custemail = db.Column('cust_email', db.Date)
    custPhone = db.Column('cust_phone', db.String(50))
    custInZone = db.Column('cust_in_zone', TINYINT)
    addr_id = db.Column(db.Integer, db.ForeignKey(Address.addr_id))
    login = db.relationship("Login", backref="placement_hash")


def __init__(self, custfirstname, custlastname, custdob, custemail, custInZone, custphone):
    self.custfirstname = custfirstname
    self.custlastname = custlastname
    self.custdob = custdob
    self.custemail = custemail
    self.custInZone = custInZone
    self.custphone = custphone


def __repr__(self):
    return '<User first name is %r>' % self.custfirstname
