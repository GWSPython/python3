class RomanNumeral:
    def int_to_Roman(self, num):
        val = [
            1000, 900, 500, 400,
            100, 90, 50, 40,
            10, 9, 5, 4
        ]

        syb = [
            "M", "CM", "D", "CD",
            "X", "IX", "V", "IV"
            "C", "XC", "L", "XL",
        ]

