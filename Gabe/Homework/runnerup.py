def raw_input():
    if __name__=='__main__':
        nums = map(int, input().split(","))
        print(sorted(list(nums))[-2])


raw_input()
