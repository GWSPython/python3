# this program adds the last digit and prints the sum of the previous one
def fibonacci(n):
        if n <= 1:
            return n
        else:
            return (fibonacci(n - 1) + fibonacci(n - 2))

terms = 8

for i in range(terms):
    print(fibonacci(i))
