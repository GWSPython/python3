def even_odd_check():
    print("Please Enter a number")
    n = int(input().strip())
    print("Number Entered is {:d}".format(n))

    if n < 1 or n > 100:
        print("Number not acceptable ")

    if n % 2 == 1:
        print("Weird")

    if n % 2 == 0:
        if n >= 2 and n <= 5:
            print("not weird")

        if n >= 6 and n <= 20:
            print("Weird")

        if n > 20 and n <= 100:
            print("Not Weird")

even_odd_check()