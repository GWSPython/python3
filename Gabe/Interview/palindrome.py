languages = ["malayalam", "spanish"]

for language in range(0, languages.__len__()):
    reverString = ""
    palString = languages.__getitem__(language)
    reverString = palString[::-1]  # reverse the string from back
    if (reverString == palString):
        print(palString + " is a palindrome")
    else:
        print(palString + " is not a palindrome")
