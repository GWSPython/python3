FastfoodList = ['Wendys', 'Mcdonalds', 'Whataburger', 'Burger King']
for food in FastfoodList:
    print(food)
for food in FastfoodList[:-1]:
    print(food)

FastfoodList.append("Canes")
for food in FastfoodList:
    print(food, end=',')

FastfoodList = FastfoodList + ["JackintheBox", "Panera", "Bojangles", "KFC"]
print()
FastfoodList.sort()
for food in FastfoodList:
    print(food, end=',')

print()
FastfoodList.sort(reverse=True)
for food in FastfoodList:
    print(food, end=',')

FastfoodList = FastfoodList * 2
print()
print(FastfoodList.__len__())
for food in FastfoodList:
    print(food, end=',')
