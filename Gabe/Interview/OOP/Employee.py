class Employee:
    companyname = 'ccc'  # Class attribute this is common for all employees

    def __init__(self, firstname, lastname, pay):  # this is a method
        self.first = firstname  # these are instance attributes
        self.last = lastname
        self.salary = pay

    def calculateBonus(self):  # method
        return self.salary * 0.5  # this method returns


# emplyee1 and employee2 are objects created from Employee class
employee1 = Employee('Gabe', 'Oates', 80000)
employee2 = Employee('Rohan', 'Perumalil', 50000)

print(employee1.first)
print(employee1.calculateBonus())
print(employee2.first)

print(employee2.calculateBonus())
print(employee1.companyname)
print(employee2.companyname)
