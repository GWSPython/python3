class Book:  # parent class

    def __init__(self, ISBN, name, author, price=10):
        self.isbn = ISBN
        self.bookName = name
        self.bookAuthor = author
        self.price = price

    def lateFee(self):
        lateFee = 0
        if (self.price < 50):
            lateFee = 1
        elif (self.price > 50 and self.price < 101):
            lateFee = 4
        return lateFee


# child class, it has same attributes as the parent class no need to add again
# Childrenbook inherited properties from parent Book
class ChildrenBook(Book):

    def lateFee(self):  # chilren's book late fee is different so we implemented in child class differnctly
        self.lateFee = 0
        if (self.price < 50):
            self.lateFee = 3
        elif (self.price > 50 and self.price < 101):
            self.lateFee = 10
        return self.lateFee


if __name__ == '__main__':
    book1 = Book('1234', 'Lord of the rings', 'John Smith', 100)
    book3 = ChildrenBook('9087', 'Elmo the monster', 'Gabe Oates')  # inherit from parent
