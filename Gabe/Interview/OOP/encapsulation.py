# alldata changes should happen in class
# increase the price through behavior on an object
class Book:

    def __init__(self, ISBN, price):
        self.___isbn = ISBN
        self.___price = price

    def getIsbn(self):
        return self.___isbn

    def setIsbn(self, isbn):
        isbn.upper()
        self.___isbn = isbn


last_season_book = Book('978-0060583002', 200)
# last_season_book.____isbn ___isbn  not accesible  - THIS ENCAPSULATION
last_season_book.setIsbn('777-006058h3002')

print(last_season_book.getIsbn())
