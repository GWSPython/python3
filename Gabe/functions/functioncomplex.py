days_In_Month = [0, 31, 28, 30, 31, 30, 31, 30, 31, 30, 31, 30, 31]


def days_in_Leap_Year(year):
    return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)


def days_in_a_month(year, month):
    if not (month > 0 and month < 13):
        return "invalid month"
    if month == 2 and days_in_Leap_Year(year):
        return 29
    else:
        return days_In_Month[month]
print(days_in_a_month(2019, 6))
print(days_in_a_month(2021, 13))
print(days_in_a_month(2024,2))
