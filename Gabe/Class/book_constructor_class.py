class Book:
    def __init__(self):
        print("book instance created")

beachglass_book = Book()
beachglass_book.ISBN = '978-0312351588'
beachglass_book.name = 'Beachglass'
beachglass_book.publishYear = '2019'

step_on_a_crack_book = Book()
step_on_a_crack_book.ISBN = '978-0316013949'
step_on_a_crack_book.name = 'Step on a Crack'
step_on_a_crack_book.publishYear = '2004'

print(beachglass_book)
print(step_on_a_crack_book)
