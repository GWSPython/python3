class Book:
    def __init__(self, ISBN, price):
        self.ISBN = ISBN
        self.price = price
        print("book instance created")

    def decreasePrice(self, decreaseby):

        if (self.price - decreaseby) > 0:
            self.price = self.price - decreaseby


beachglass_book = Book('978-0312351588', 200)
beachglass_book.name = 'Beachglass'
beachglass_book.publishYear = '2019'
#beachglass_book.price = beachglass_book.price - 500
beachglass_book.decreasePrice(500)

step_on_a_crack_book = Book('978-0316013949', 150)
step_on_a_crack_book.name = 'Step on a Crack'
step_on_a_crack_book.publishYear = '2004'
step_on_a_crack_book.price += 5

print(beachglass_book.price)
print(step_on_a_crack_book.price)
