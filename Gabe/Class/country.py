class Country:
    pass

america = Country()
india = Country()
england = Country()

america.name = 'USA'
america.capital = 'Washington D.C.'

india.name = 'India'
india.capital = 'New Delhi'

england.name = 'Great Britain'
england.capital = 'London'

print(india.capital)
print(america.capital)
print(england.capital)

india.capital = 'Delhi'
