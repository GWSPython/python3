class Book:

    def __init__(self, ISBN, price):
        self.___isbn = ISBN
        self.___price = price

    def getISBN(self):
        return self.___isbn

    def setISBN(self, isbn):
        isbn.upper()
        self.___isbn = isbn


last_season_book = Book("978-0060529002", 200)
# last_season_book.___isbn ___isbn not accessible

last_season_book.setISBN('777-0060583002')

print(last_season_book.getISBN())
