class Book:
    def __init__(self, id, name, author):
        self.id = id
        self.name = name
        self.author = author
        self.reviews = []

    def add_review(self, review):
        self.reviews.append(review)

    def __repr__(self):
        return repr((self.name, self.author, self.id, self.reviews))

class Review:
    def __init__(self, id, description, rating):
        self.id = id
        self.description = description
        self.rating = rating

    def __repr__(self):
        return repr((self.description, self.rating, self.id))

if __name__ == '__main__':
    book1 = Book(1, 'Lord of the Rings', 'James Curry');
    review = Review(1, "good book", 1)
    review1 = Review(1, "ok book", 3)
    book1.add_review(review)
    book1.add_review(review1)
    print(book1)