class LandAnimal:
    def __init__(self):
        super().__init__()
        self.walkingspeed = 2

    def _increasewalkingspeed(self, howmuch):
        self.walkingspeed += howmuch

class WaterAnimal:
    def __init__(self):
        super().__init__()
        self.swimmingspeed = 3

    def _increaseswimmingspeed(self, howmuch):
        self.swimmingspeed += howmuch

class Amphibian(LandAnimal, WaterAnimal):
    def __init__(self):
        super().__init__()

if __name__ == '__main__':
    a1 = Amphibian()
    a1._increasewalkingspeed(4)
    a1._increaseswimmingspeed(5)
    print(a1.swimmingspeed)
    print(a1.walkingspeed)
