class Student:

    def __init__(self, rollno, name):
        self.name = name
        self.rollno = rollno
        self.laptop = self.Laptop()

    def show(self):
        print(self.name, self.rollno, self.laptop.brand, self.laptop.os)

    class Laptop:
        def __init__(self):
            self.brand = "HP"
            self.os = "Windows"

student = Student(1, 'smitha')
student.show()

laptop = Student(2, 'windy').Laptop()
print(laptop.brand, laptop.os)