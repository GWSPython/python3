from abc import ABC, abstractmethod

class AbstractAnimal(ABC):
    @abstractmethod
    def bark(self): pass


class Doberman(AbstractAnimal):
    def bark(self):
        print("ruf ruf ruf")

class Lab(AbstractAnimal):
    def bark(self):
        print("ruf")

print(Doberman().bark())
print(Lab().bark())
