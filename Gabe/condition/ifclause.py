if True:
    print("it is true")

if False:
    print("it is false")

country = "USA"

if country == "China" or country == "India":
    print("Continent is Asia")
else:
    print("Continent is North America")

hair = "black"
eyes = "brown"

if hair == "black" and eyes == "black":
    print("You are asian")

if hair == "black" and not eyes == "black":
    print("you are white")

x = 100
y = 80
z = 45

if x > y:
    print("x is greater than y")
if z < x:
    print("z is less than x")
if x > y and x > z:
    print("x is greater than y and z")

p = 110

if y < z or y < p:
    print("y is less than one of the values")

q = 45

if z <= q:
    print("z is less than or equal to q")
if z != y:
    print("z is not equal to y")
condition = False

if condition:
    print("Evaluated to True")
else:
    print("Evaluated to false")

condition = None
if condition:
    print("Evaluated to True")
else:
    print("Evaluated to False")

condition = []
if condition:
    print("Evaluated to True")
else:
    print("Evaluated to False")
