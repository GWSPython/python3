def read_file():
    f = open('test.txt', 'r')
    print(f.name)
    f.close()


read_file()


def read_file_read_method():
    with open('test.txt', 'r') as f:
        f_contents = f.read()
        print(f_contents)
        f.close()


read_file_read_method()


def read_file_read_with_char_size_method():
    with open('test.txt', 'r') as f:
        f_contents = f.read(100)
        print(f_contents)
        print('*****************************')
        f_contents = f.read(100)
        print(f_contents)
        f.close()


read_file_read_with_char_size_method()


def read_lines():
    with open('test.txt', 'r') as f:
        f_contents = f.readlines()
        print(f_contents)
        f.close()


read_lines()


def read_line():
    with open('test.txt', 'r') as f:
        f_contents = f.readline()
        print(f_contents)
        f.close()
print('************')


read_line()

def read_line_through_loop():
    with open('test.txt', 'r') as f:
        for line in f:
            print(line)
        f.close()

read_line_through_loop()
