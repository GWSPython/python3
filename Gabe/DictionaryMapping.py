address = {"Gabe": "1234 Georgetown", "Windy": "4567 Austin", "Smitha": "6789 Austin"}
print("Gabe's Address is "+ address["Gabe"])
print("Windy's Address is "+ address["Windy"])

record = {"name": {'firstname': "Stephen", "middlename": "Gabe", "lastname": "Oates"},
          "job": ["developer", "devops"],
          "age": 24}

print(record["name"])
print(record["name"]["lastname"])
print(record['job'])
print(record['job'].append('dba'))
print(record['job'])
print(record['age'])

if not 'address' in record:
    print("missing")
if 'job' in record:
    print("not missing")

states = {"TX": "Texas", "NY": "New York", "CO": "Colorado"}
keylist = list(states.keys())
keylist.sort()
print(keylist)

for key in keylist:
    print(key, "===> ", states[key])
