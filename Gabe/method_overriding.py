class Car:
    def sound(self):
        print("Vroom")

    def drive(self):
        print("2 wheel drive")

    def mpg(self):
        print("Good gas mileage")

class Truck(Car):
    def drive(self):
        print("4 wheel drive")

    def mpg(self):
        print("Bad gas mileage")

if __name__ == '__main__':
    t = Truck()
    t.sound()
    t.drive()
    t.mpg()