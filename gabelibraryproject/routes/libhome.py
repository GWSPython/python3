from flask import render_template, url_for, flash, redirect, request, session
from gabelibraryproject import app, db
from gabelibraryproject.model import loginmodel
from gabelibraryproject.model.customermodel import Customer
from gabelibraryproject.routes.loginform import LoginForm
from gabelibraryproject.routes.resgisterform import RegistrationForm
from gabelibraryproject.model import addressmodel
from gabelibraryproject.model.loginmodel import Login
from gabelibraryproject.model.addressmodel import Address
from gabelibraryproject.routes.searchForm import SearchForm
from gabelibraryproject.model.authorModel import Author
from gabelibraryproject.model.bookModel import Book
from sqlalchemy import insert
from sqlalchemy.sql import text


@app.route("/home", methods=['GET', 'POST'])
def home():
    if "loginUser" in session:
        username = session['loginUser']
        data = "Hello " + username;
        return render_template('homePage.html', value=data)
    else:
        return render_template('homePage.html')


@app.route("/contact", methods=['GET', 'POST'])
def contact():
    return render_template('contact.html')


@app.route("/search", methods=['GET', 'POST'])
def search():
    SearchData = SearchForm(request.form)

    str = text("''")

    if (SearchData.authorSearch.data != ''):
        str = text('Author.author_first_name like ' + "'%" + SearchData.authorSearch.data +"%'")
    if (SearchData.bookSearch.data != ''):
        str = text('Book.title like ' + "'%" + SearchData.bookSearch.data +"%'")

    if request.method == 'POST':
        searchresult = db.session.query(Book.title, Book.noofcopies, Author.authorFname, Author.authorLname).join(
            Author, Author.ISBN == Book.ISBN) \
            .filter(str).all()
        return render_template('search.html', len=len(searchresult), searchresult=searchresult)

    return render_template('search.html', form=SearchForm)


@app.route("/register", methods=['GET', 'POST'])
def register():
    registerform = RegistrationForm(request.form)
    if request.method == 'POST' and registerform.validate_on_submit():
        address = Address(registerform.addr1.data, registerform.city.data, registerform.state.data, '89776')
        customer = Customer(firstname=registerform.firstname.data,
                            lastname=registerform.lastname.data,
                            dob=registerform.dob.data,
                            email=registerform.email.data,
                            custInZone=1,
                            phone=registerform.phone.data)
        login = Login(
            username=registerform.firstname.data[0].lower() + registerform.lastname.data.lower(),
            password=registerform.password1.data)
        session["loginUser"] = registerform.firstname.data[0].lower() + registerform.lastname.data.lower()
        customer.login.append(login)
        address.customer.append(customer)
        db.session.add(address)
        db.session.commit();

        return home()
    return render_template('registration.html', form=registerform)




@app.route("/login", methods=['GET', 'POST'])
def login1():
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate_on_submit():
        # session.__setattr__("loginUser", form.username.data)
        # login = loginmodel.Login.query.filter_by(username=form.username.data, password=form.password.data).first()
        # customer = customermodel.Customer.query.filter_by(cust_id=login.cust_id).first()
        userinputlogin = Login(form.username.data, form.password.data)

        result = db.session.query(Login, Customer).join(Customer, Login.cust_id == Customer.cust_id) \
            .filter(Login.username == userinputlogin.username, Login.password == userinputlogin.password).first()

        if result:
            session["loginUser"] = result.Customer.firstname + " " + result.Customer.lastname
            return home()
        else:
            if "loginUser" in session:
              session.pop("loginUser")
            return "Login failed"

    else:
        return render_template('login.html', title="Login", form=form)


if __name__ == '__main__':
    app.run(debug=True)
