from wtforms.validators import DataRequired, Length
from wtforms import StringField
from flask_wtf import FlaskForm

class SearchForm(FlaskForm):
    authorSearch = StringField("authorSearch")
    bookSearch = StringField("bookSearch")