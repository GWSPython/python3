from wtforms.validators import DataRequired, Length
from wtforms import StringField, IntegerField
from flask_wtf import FlaskForm


class SearchResult(FlaskForm):
    title = StringField("Title")
    author = StringField("author")
    copies = IntegerField("copies")

    def __init__(self, title, author, copies):
        self.title = title
        self.author = author
        self.copies = copies
