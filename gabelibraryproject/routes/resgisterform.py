from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, DateField
from wtforms.validators import DataRequired, Length


class RegistrationForm(FlaskForm):
    firstname = StringField('firstname')
    lastname = StringField('lastname')
    dob = DateField('dob')
    email = StringField('email')
    addr1 = StringField('Address1')
    addr2 = StringField('Address2')
    city = StringField('city')
    state = StringField('state')
    phone = StringField('phone')
    zip = StringField('zip')
    password1 = PasswordField('password1')
    password2 = PasswordField('password2')
    submit = SubmitField('Create Account')

    # firstname = StringField('firstname',
    #                         validators=[DataRequired()])
    # lastname = StringField('lastname', validators=[DataRequired()])
    # dob = DateField('dob')
    # email = StringField('email', validators=[DataRequired])
    # addr1 = StringField('Address1', validators=[DateField])
    # addr2 = StringField('Address2')
    # city = StringField('city', validators=[DataRequired])
    # state = StringField('state', validators=[DataRequired])
    # phone = StringField('phone', validators=[DataRequired])
    # password1 = PasswordField('password1', validators=[DataRequired()])
    # password2 = PasswordField('password2', validators=[DataRequired()])
    # submit = SubmitField('Create Account')

