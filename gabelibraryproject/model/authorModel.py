from gabelibraryproject import db
from gabelibraryproject.model.bookModel import Book


class Author(db.Model):
    author_id = db.Column('author_id', db.Integer, primary_key="true")
    authorFname = db.Column('author_first_name', db.String(50))
    authorLname = db.Column('author_last_name', db.String(50))
    ISBN = db.Column(db.String(20), db.ForeignKey(Book.ISBN))

    def __init__(self, author_id, authorFname, authorLname, ISBN):
        self.author_id = author_id
        self.authorFname = authorFname
        self.authorLname = authorLname
        self.ISBN = ISBN

