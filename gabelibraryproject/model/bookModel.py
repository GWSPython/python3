from gabelibraryproject import db

class Book(db.Model):
    ISBN = db.Column('ISBN', db.String(20), primary_key="true")
    title = db.Column('title', db.String(100))
    pub_year = db.Column('pub_year', db.Integer)
    #price = db.Column('price', db.Decimal(5, 2))
    noofcopies = db.Column('no_of_copies', db.Integer)
    author = db.relationship("Author", backref="placement_hash")

    def __init__(self, title, ISBN, pub_year, noofcopies):
        self.ISBN = ISBN
        self.title = title
        self.pub_year = pub_year
        self.price = noofcopies


