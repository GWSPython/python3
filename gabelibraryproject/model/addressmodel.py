from gabelibraryproject import app, db


class Address(db.Model):
    addr_id = db.Column('addr_id', db.Integer, primary_key='true')
    street = db.Column('addr_street', db.String(50))
    city = db.Column('addr_city', db.String(10))
    state = db.Column('addr_state', db.String(10))
    zip = db.Column('addr_zip', db.String(10))
    customer = db.relationship("Customer", backref="placement_hash")

    def __init__(self, street, city, state, zip):
        self.street = street
        self.city = city
        self.state = state
        self.zip = zip

    def __repr__(self):
        return '<User smitha is %r>' % self.street
