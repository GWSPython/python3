from gabelibraryproject import app, db
from gabelibraryproject.model.addressmodel import Address
from sqlalchemy.dialects.mysql import MEDIUMINT, TINYINT


class Customer(db.Model):
    pass
    cust_id = db.Column('cust_id', db.Integer, primary_key='true')
    firstname = db.Column('cust_first_name', db.String(50))
    lastname = db.Column('cust_last_name', db.String(10))
    dob = db.Column('cust_dob', db.Date)
    email = db.Column('cust_email', db.String(50))
    phone = db.Column('cust_phone', db.String(50))
    custInZone = db.Column('cust_in_zone', TINYINT)
    addr_id = db.Column(db.Integer, db.ForeignKey(Address.addr_id))
    login = db.relationship("Login", backref="placement_hash")


    def __init__(self, firstname, lastname, dob, email, phone, custInZone):
        self.firstname = firstname
        self.lastname = lastname
        self.dob = dob
        self.email = email
        self.phone = phone
        self.custInZone = custInZone

    def __repr__(self):
        return '<User smitha is %r>' % self.firstname
