from flask import Flask, render_template, url_for, flash, redirect, request
app = Flask(__name__)
@app.route("/home", methods=['GET', 'POST'])
def home():
    return render_template('HomePage.html')

@app.route("/contact", methods=['GET', 'POST'])
def contact():
    return render_template('Contact.html')

@app.route("/register", methods=['GET', 'POST'])
def register():
    return render_template('registration.html')

if __name__ =='__main__':
    app.run(debug=True)