if True:
    print("This is true.")

if False:
    print ("This is false.")

Country = "China"
if Country == ("China" or "India"):
    print ("This is AWESOME!")

Country = "Taiwan"
if Country == ("China" or "India"):
    print("Awesome countries.")
else:
    print("Boo")

Eye = "Black"
Hair = "Black"
if Eye=="Black" and Hair == "Black":
    print("Awesome Asians")
if Eye == "Black" and not Hair == "Blue":
    print ("Black eyes!!")

x=100
y=80
z=45

if x>y:
    print ("X is greater than y.")
if z<x:
    print ("Z is less than x.")
if x>y and x>z:
    print ("X is greater than Y and Z.")

p=110
if y<z or y<p:
    print("Y is less than one of them: z or P.")

q=45
if z<=q:
    print("Z is less or equal to q.")

if z!=y:
    print ("z does not equal to y.")

condition = False
if condition:
    print ("It is true.")
else:
    print("It is false.")


condition = None
if condition:
    print("It is true.")
else:
    print("It is false.")

condition = [] #empty list
if condition:
    print("It is true.")
else:
    print("It is false.")

condition = ''
if condition:
    print("It is true.")
else:
    print("It is false.")

condition = 0
if condition:
    print("It is true.")
else:
    print("It is false.")



