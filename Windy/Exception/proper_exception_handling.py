def file_manipulation():
    try:
        f = open("this_file_does_not_exit.txt")
        v = jijnk
    except Exception:
        print ("file not found.")
# file_manipulation()

def file_manipulation1():
    try:
        f = open("this_file_does_exit.txt")
        v = jijnk
    except FileNotFoundError:
        print ("File not found")
    except Exception:
        print ("something went wrong")
    else:
        print (f.read)
        f.close()
    finally:
        print ("I am running this regardless")
# file_manipulation1()


def file_manipulation2():
    try:
        f = open("this_file_does_exit.txt")
        v = "2dtty"
    except FileNotFoundError as e:
        print (e)
    except Exception:
        print ("something went wrong")
    else:
        f_contents = f.readline()
        print (f_contents)
        f.close()
    finally:
        print ("I am running this regardless")
file_manipulation2()