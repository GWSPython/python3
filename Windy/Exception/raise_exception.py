def file_manipulation3():
    try:
        f = open("corruptfile.txt")
        if f.name == "corruptfile.txt":
            raise Exception
    except FileNotFoundError:
        print ("File corrupted.")
    except Exception:
        print ("Error encountered due to corrupted file.")
    else:
        print (f.read)
        f.close()
    finally:
        print ("I am running this regardless")
file_manipulation3()