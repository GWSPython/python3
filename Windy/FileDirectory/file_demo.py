def read_file():
    f = open("test.txt", 'r')
    print(f.name)
    f.close()


read_file()


def read_file_read_method():
    with open("test.txt", 'r') as w:
        w_content = w.read()
        print(w_content)
        w.close()


read_file_read_method()

def read_file_with_char_size_mothod():
    with open("test.txt",'r')as w:
        w_content = w.read(10)
        print(w_content)
        print("***********")
        w_content = w.read(10)
        print(w_content)
        w.close()

read_file_with_char_size_mothod()

def read_by_line():
    with open("test.txt",'r')as w:
        w_content=w.readline()
        print(w_content)
        w.close()
read_by_line()

def read_by_lines():
    with open("test.txt",'r')as w:
        w_content=w.readlines()
        print(w_content)
        w.close()
read_by_lines()

def read_line_loop():
    with open("test.txt",'r')as w:
        for line in w:
            print(line)
    w.close()
read_line_loop()