class OpeOverloading:
    def __init__(self,history,physics):
        self.history=history
        self.physics=physics

    def __mul__(self, p1):
        self.history= (self.history + p1.history)//2
        self.physics = (self.physics + p1.physics)//2
        h3 = OpeOverloading(self.history,self.physics)
        return h3


    def __repr__(self):
        return repr((self.history,self.physics))

if __name__=='__main__':
    a=5
    b=8
    print(a*b)

    x=5.2
    y=8.4
    print(x*y)

    h1=OpeOverloading(90,92)
    print(h1)

    h2=OpeOverloading(80,60)
    print (h2)

    h3=h1*h2
    print(h3)