class Country:
    pass
# object of the class
american = Country()
india = Country()
china = Country()

# state of the objects
american.name = 'USA'
american.capital = "Washington DC"
india.name = "Inida"
india.capital = "New Delhi"
china.name = "China"
china.capital = "Beijing"

print (american.capital)
print (india.capital)
print (china.name)
