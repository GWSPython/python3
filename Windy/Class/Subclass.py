class Person:
    def __init__(self, name, job=None, pay=0):
        self.name=name
        self.job=job
        self.pay=pay

    def lastname(self):
        return self.name.split()[1]

    def giveraise(self,percent):
        self.pay = int(self.pay*(1+percent))

    def __repr__(self):
        return repr((self.name, self.job,self.pay))

class Manager(Person):
    def __init__(self,name,pay):
        Person.__init__(self, name, 'mgr',pay)

    def giveraise(self,percent, bonus=.10):
        Person.giveraise(self, percent+bonus)

if __name__=='__main__':
    bob = Person('Bob Smith')
    sue= Person ('Sue Jones', job='dev', pay=18000)
    print (bob)
    print(sue)
    print(bob.lastname(),sue.lastname())
    sue.giveraise(.10)
    print(sue)
    tom=Manager('Tom Jones', 29000)
    tom.giveraise(.10)
    print(tom.lastname())
    print(tom)