from abc import ABC, abstractmethod

class AbstractAnimal(ABC):
    @abstractmethod
    def bark(self):pass

class Poodle(AbstractAnimal):
    def bark(self):
        print ("ruf ruf ruf")

class Lab(AbstractAnimal):
    def bark(self):
        print("Ruf")

print(Poodle().bark())
print(Lab().bark())