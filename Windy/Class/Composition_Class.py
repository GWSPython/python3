class Book:
    def __init__(self, id ,name,author):
        self.id = id
        self.name = name
        self.author = author
        self.reviews =[]
        # put this in the contructor

    def add_review (self, review):
        self.reviews.append(review)

    def __repr__(self):
        return repr((self.author,self.name,self.id,self.reviews))

class Review:
    def __init__(self,id,description,rating):
        self.id = id
        self.description = description
        self.rating = rating

    def __repr__(self):
        return repr((self.id,self.description,self.rating))

if __name__ == '__main__':
    book1=Book(1,'Loard of the ring','James Curry')
    review1 = Review (1, "AWESOME", 5)
    review2 = Review (1, "Don't like this book", 2 )
    book1.add_review(review1)
    book1.add_review(review2)
    print(book1)
