class book:
    def __init__(self, ISBN, price):
        self.isbn = ISBN
        self.price = price
        print("Constructor is called")

    def decrease_price(self, decreaseby):
        if self.price - decreaseby > 0:
            self.price = self.price - decreaseby

beach_glass = book("978-0312351588", 200)
beach_glass.name = "Beachglass"
beach_glass.publishyear = "2019"
# beach_glass.price -= 500
beach_glass.decrease_price(500)

sisters = book("978-0385340229", 80)
sisters.name = "Sisters"
sisters.publishyear = "1999"


# print (beach_glass)
# print (sisters)
print(beach_glass.price)
print(sisters.name)
