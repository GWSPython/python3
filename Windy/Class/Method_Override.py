class Vehicle:
    def sound(self):
        print("honk honk")
    def drive(self):
        print("2WD")

class Car(Vehicle):
    def sound(self):
        super().sound()
    def drive(self):
        super().drive()
        print ("4WD")

if __name__=='__main__':
    v=Car()
    v.sound()
    v.drive()
