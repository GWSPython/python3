class Student:
    def __init__(self,studentno,name):
        self.studentno=studentno
        self.name=name
        self.laptop=self.Laptop()

    def show(self):
        print(self.name,self.studentno,self.laptop.brand,self.laptop.os)

    class Laptop:
        def __init__(self):
            self.brand = 'HP'
            self.os='windows'
student = Student(1,'Smitha')
student.show()

laptop = Student (2,"Windy").Laptop()
print(laptop.brand,laptop.os)
