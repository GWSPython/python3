eyecolor = {"Christine":"Brown","Glenn":"Green","Max":"Blue","Windy":"Brown"}

print("Christine's eye color is " + eyecolor["Christine"])
print("Glenn's eye color is "+eyecolor["Glenn"])

insurance = {'Vehicle': {"Year": "2019","Brand": "Buick","Model": "Enclave"},
             'Insurer': ['Windy Sampson', "Glenn Sampson"],
             'creditscore': 820}

print(insurance["Vehicle"])
print(insurance["Vehicle"]["Brand"])
print(insurance["Insurer"])
insurance['Insurer'].append('Windy Wu')
print(insurance['Insurer'])
print(insurance['creditscore'])

#always good to check to avoid NPE
if not 'height' in eyecolor:
    print("This record does not contain height information")
if 'Glenn' in eyecolor:
    print ("Glenn has "+eyecolor["Glenn"]+ " eyes.")

#play around with keylist

subjects = {"2": "Math", "1": "Reading", "3": "Writing"}

scores = list(subjects.keys())
scores.sort()
print(scores)

for keys in scores:
    print(keys," is the subject number for",subjects[keys])
