def first_func():
    print("First Function!")


first_func()

print("First Function!")
print("First Function!")
print("First Function!")


def oo_func():
    return "First Function!!!!"


print(oo_func())


def greeting_func(greetings, name):
    return greetings + " " + name


print(greeting_func("Today", "Hot"))


def greeting_func1(greetings, name='Windy'):
    return '{},{}'.format(greetings,name)


print(greeting_func1("Today"))


def student(*args,**kwargs):
    print(args)#tuple
    print(kwargs)#dict

student("Math","Statistics","Art", name="Gabe Oates",major="Mathematics",minor="statistics",gpa="4.0")

sub = ["Math","Statistics"]
student1 = {'name':"Gabe Oates",'major':"Mathematics",'minor':"statistics"}
student(sub,student1)

student(*sub,**student1)
