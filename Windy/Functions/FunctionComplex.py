days_In_Month = [0, 31, 28, 30, 31, 30, 31, 30, 31, 30, 31, 30, 31]

def is_leap_year(year):
    return year % 4 == 0 and (year % 100 !=0 or year %400 == 0)

def days_in_the_month(year, month):
    if (month<1 or month >13):
        return ("Invalid Month")
    if month == 2 and is_leap_year(year):
        return 29
    else:
        return (days_In_Month[month])


print(days_in_the_month(2019,2))
print(days_in_the_month(2020,2))
print(days_in_the_month(2018,5))
print(days_in_the_month(2019,30))
