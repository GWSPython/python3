def even_odd_check():
    print("Please Enter a number")
    n = int(input().strip())
    print("Number Entered is {:d}".format(n))

    if n % 2 == 1:
        print("Weird that this number is odd.")
    elif n>=2 and n<=5:
        print("Not Weird")
    elif n>=6 and n<=20:
        print("Weird")
    elif n>20:
        print("Not Weird")

even_odd_check()

#Given an integer, , perform the following conditional actions:
# If n is odd, print Weird
# If n is even and in the inclusive range of 2 to 5 , print Not Weird
# If n is even and in the inclusive range of  6 to 20, print Weird
# If n is even and greater than 20, print Not Weird
# number should be greater than 1 less than or equal to 100.
# your code should be here




