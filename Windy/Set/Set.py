subjectSet = {"History","Math","Biology","Zoology","Math","Chemistry"} #run multiple times order changes
artsubjectSet = {"History","Art","Biology","Zoology","Math"}
print(subjectSet)

print(subjectSet.intersection(artsubjectSet))
print(subjectSet.difference(artsubjectSet))
print(subjectSet.union(artsubjectSet))

print(artsubjectSet.intersection(subjectSet))
print(artsubjectSet.difference(subjectSet))
print(artsubjectSet.union(subjectSet))

print("Art" in artsubjectSet)