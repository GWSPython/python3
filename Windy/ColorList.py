ColorList = ["Red","Orange","Blue","Yellow"]
for color in ColorList:
    print(color)

for color in ColorList[:-1]:
    print(color)

#append only adds one
ColorList.append("Black")
for color in ColorList:
    print(color)

#while using this, you can add multiple. I wonder how to add something in the middle but not at the end of the list
ColorList = ColorList + ["Neon","Rainbow"]
for color in ColorList:
    print(color)

#I tried to use print() here but it didn't print in sorting order. sometimes you can use print() and sometimes print (color). Why?
ColorList.sort()
for color in ColorList:
    print(color)

ColorList.sort(reverse=True)
for color in ColorList:
    print(color)

print(ColorList.__len__())

ColorList=ColorList*10
print(ColorList.__len__())

for color in ColorList:
    print(color, end=".")

