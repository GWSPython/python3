#1. Write a Python program to find those numbers which are divisible by 7 and multiple of 5, between 1500 and 2700 (both included).

number_list = []
for num in range (1500,2700):
    if (num%7==0) and (num%5 == 0):
        number_list.append(str(num))
print (','.join(number_list))


## 2  Write a Python program to convert temperatures to and from celsius, fahrenheit.
# temp = input("Do you have Celsius or Fahrenheit--Please put C or F:")
# degree = int(input("Please put in degree: "))
## no error handling of degree being non-numberic
# if temp == 'C':
#     degree_f = (degree * 9/5) + 32
#     print ("The temperature in fahrenheit is "+ str(degree_f))
# elif temp == 'F':
#     degree_c = (degree - 32) *5/9
#     print("The temperature in celsius is "+ str(degree_c))
# else:
#     print ("Please give the correct input--Upper case C or F")

#3 Write a Python program to guess a number between 1 to 9. Go to the editor.
# Note : User is prompted to enter a guess. If the user guesses wrong then the prompt appears again until the guess is correct,
# on successful guess, user will get a "Well guessed!" message, and the program will exit.
#Learning from this:
##did not know about import random
##Should use a while loop instead of if
#import random
#number,guess = random.randint(0,9),0 #What is this syntax?
#Also missing exception handling if input is string/alphabetical
#while number!=guess:
#    guess = int(input("Please guess a number (0-9):"))
#print("Well Guessed!")


#4  Write a Python program to construct the following pattern, using a nested for loop.
n = 5
for i in range (n): # i starts as 0, then 1, 2, 3, 4
    for j in range (i): # j starts as 0, then 1, 2, 3, 4
        print ("*", end = "")
    print ("") # this serves as return? I thought \n does that, but \n gives two lines apart

for i in range (n,0,-1):
    for j in range (i):
        print ("*", end = "")
    print ("")

## 5 Write a Python program that accepts a word from the user and reverse it.
# name_to_reverse = input("Please enter one word: ")
# for nam_1 in range(name_to_reverse.__len__()-1,-1,-1):
    # print(name.__getitem__(nam))
#    print(name_to_reverse[nam_1],end="")

# 6. Write a Python program to count the number of even and odd numbers from a series of numbers.
number_count_even_odd = (1, 2, 30, 4, 5, 6, 8, 10, 7)
even=0
odd=0
for num_even_odd in range (number_count_even_odd.__getitem__(8)):
    if num_even_odd%2==0:
        even=even+1
    else:
        odd=odd+1
print("You have "+str(even)+" even numbers and "+str(odd)+" odd numbers.")

# 7. Write a Python program that prints each item and its corresponding type from the following list.
datalist = [1452, 11.23, 1+2j, True, 'w3resource', (0, -1), [5, 12], {"class":'V', "section":'A'}]
for item in datalist:
    print ("type of ", item, " is ", type (item))

# 8. Write a Python program that prints all the numbers from 0 to 6 except 3 and 6.
# Note : Use 'continue' statement.
# so continue means do nothing go to the next step/line
for skip_num in range (6):
    if (skip_num == 3 or skip_num == 6):
        continue
    print(skip_num, end="")
print("\n")
# 9. Write a Python program to get the Fibonacci series between 0 to 50.

# 10. Write a Python program which iterates the integers from 1 to 50.
# For multiples of three print "Fizz" instead of the number and for the multiples of five print "Buzz".
# For numbers which are multiples of both three and five print "FizzBuzz".

for integers_1_to_50 in range (0, 51):
    if integers_1_to_50%5==0 and integers_1_to_50%3==0:
        print("Fizzbuzz")
    elif integers_1_to_50%5==0:
        print("Buzz")
    elif integers_1_to_50%3 == 0:
        print ("Fizz")
    else:
        print (integers_1_to_50)

# 11. Write a Python program which takes two digits m (row) and n (column) as input and generates a two-dimensional array.
# The element value in the i-th row and j-th column of the array should be i*j.

# 12. Write a Python program that accepts a sequence of lines (blank line to terminate) as input
# and prints the lines as output (all characters in lower case).



