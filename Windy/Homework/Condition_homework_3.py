# # 31. Write a Python program to calculate a dog's age in dog's years.
# #Note: For the first two years, a dog year is equal to 10.5 human years. After that, each dog year equals 4 human years.
# dog_year=0
# dog_age = int(input("How old is the dog in human years: "))
# if dog_age<=2:
#     dog_year=dog_age*10.5
# else:
#     dog_year=(2*10.5)+((dog_age-2)*4)
# print ("Dog year equals to: "+ str(dog_year))

# # 32. Write a Python program to check whether an alphabet is a vowel or consonant
# letter=input("please input a letter of the alphabet: ")
# if letter in ('a','e','i','o','u') or letter in ('A','E','I','O','U'):
#     print ("%s is an vowel."%letter)
# elif letter=='Y' or letter == 'y':
#     print ("Sometimes letter Y stand for vowel, sometimes consonant.")
# else:
#     print ("%s is a consonant."%letter)

# # 33. Write a Python program to convert month name to a number of days
# print ("List of months: January, February, March, April, May, June, July, August, September, October, November, December")
# day=0
# month = input ("Please enter the month--Please ensure the spelling and format match the above months in the list: ")
# if month=="January" or month == "March" or month == "May" or month == "July" or month =="August" or month =="October" or month == "December":
#     day=31
# elif month == "February":
#     day = "28 or 29"
# elif month=="April" or month=="June" or month=="September" or month=="Nobember":
#     day=30
# else:
#     print ("Please enter a valid month with the correct spelling and format.")
#     exit()
#
# print ("This month has "+str(day)+" days.")

# # 34. Write a Python program to sum of two given integers. However, if the sum is between 15 to 20 it will return 20
# i1=int(input("Please enter first integer: "))
# i2=int(input("Please enter second integer: "))
# sum=i1+i2
# if sum in range (15,21):
#     sum=20
# print ("The sum is "+ str(sum))

# # 35. Write a Python program to check a string represent an integer or not.
# check_string= input ("Please input a string: ")
# i=False
# for s in check_string:
#     if s.isdigit():
#        i=True
#        break
#
# if i:
#     print ("You input an integer.")
# else:
#     print ("You input a string.")

# # 36. Write a Python program to check a triangle is equilateral, isosceles or scalene
# # An equilateral triangle is a triangle in which all three sides are equal.
# # A scalene triangle is a triangle that has three unequal sides.
# # An isosceles triangle is a triangle with (at least) two equal sides.
#
# print ("Please input the lengths of all 3 sides of the triangle.")
# x= int (input("First side X (in cm): "))
# y= int (input("First side Y (in cm): "))
# z= int (input("First side Z (in cm): "))
#
# if x==y==z:
#     print ("This triangle is an equilateral triangle.")
# elif x!=y and y!=z and x!=z:
#     print ("This triangle is a scalene triangle.")
# elif x==y or y==z or x==z:
#     print ("This triangle is an isosceles triangle.")
#

# # 37Write a Python program that reads two integers representing a month and day and prints the season for that month and day.
# print(
#     "List of months: January, February, March, April, May, June, July, August, September, October, November, December")
# month = input("Please enter the month: ")
# day = input("Please enter the day of this month: ")
#
# if month in ('January', 'February', 'March'):
#     season = 'winter'
# elif month in ('April', 'May', 'June'):
#     season = 'spring'
# elif month in ('July', 'August', 'September'):
#     season = 'summer'
# else:
#     season = 'autumn'
#
# if (month == 'March') and (day > 19):
#     season = 'spring'
# elif (month == 'June') and (day > 20):
#     season = 'summer'
# elif (month == 'September') and (day > 21):
#     season = 'autumn'
# elif (month == 'December') and (day > 20):
#     season = 'winter'
#
# print("Season is", season)

# # 39. Write a Python program to display the sign of the Chinese Zodiac for given year in which you were born.
# year=int(input("Please enter the year you were born: "))
# if (year-2000)%12 ==0:
#     sign = 'Dragon'
# elif (year - 2000) % 12 == 1:
#     sign = 'Snake'
# elif (year - 2000) % 12 == 2:
#     sign = 'Horse'
# elif (year - 2000) % 12 == 3:
#     sign = 'sheep'
# elif (year - 2000) % 12 == 4:
#     sign = 'Monkey'
# elif (year - 2000) % 12 == 5:
#     sign = 'Rooster'
# elif (year - 2000) % 12 == 6:
#     sign = 'Dog'
# elif (year - 2000) % 12 == 7:
#     sign = 'Pig'
# elif (year - 2000) % 12 == 8:
#     sign = 'Rat'
# elif (year - 2000) % 12 == 9:
#     sign = 'Ox'
# elif (year - 2000) % 12 == 10:
#     sign = 'Tiger'
# else:
#     sign = 'Hare'
#
# print("Your Zodiac sign :",sign)
#
# # 40. Write a Python program to find the median of three values.
# v1=int(input("Please enter the first number: "))
# v2=int(input("Please enter the second number: "))
# v3=int(input("Please enter the third number: "))
#
# if v1>v2:
#     if v1<v3:
#         m=v1
#     elif v2>v3:
#         m=v2
#     else:
#         m=v3
# else:
#     if v2<v3:
#         m=2
#     elif v1>v3:
#         m=v1
#     else:
#         m=v3
#
# print ("The median is ", m)
#
# # 41. Write a Python program to get next day of a given date.
# year=int(input("Please enter the year: "))
#
# if (year % 400 == 0):
#     leap_year = True
# elif (year % 100 == 0):
#     leap_year = False
# elif (year % 4 == 0):
#     leap_year = True
# else:
#     leap_year = False
#
# month = int (input("Please enter the month (1-12): "))
#
# if month in (1, 3, 5, 7, 8, 10, 12):
#     month_length = 31
# elif month == 2:
#     if leap_year:
#         month_length = 29
#     else:
#         month_length = 28
# else:
#     month_length = 30
#
# day = int(input("please enter the day(1-31): "))
# if day < month_length:
#     day += 1
# else:
#     day = 1
#     if month == 12:
#         month = 1
#         year += 1
#     else:
#         month += 1
#
# print ("The next date is [yyyy-mm-dd]%d-%d-%d." %(year, month, day))

# # 42. Write a Python program to calculate the sum and average of n integer numbers (input from the user). Input 0 to finish.
# import statistics
# number_sum_avg = []
# sum_num=0
# count_num =0
# while True:
#     sum_avg=input("Please input your integer. Input 0 to finish: ")
#     if int(sum_avg)!=0:
#         number_sum_avg.append(sum_avg)
#         sum_num=sum_num+int(sum_avg)
#         count_num=count_num+1
#     else:
#         break;
#
# print (number_sum_avg)
# print (sum_num)
# print (sum_num/count_num)

# # 43. Write a Python program to create the multiplication table (from 1 to 10) of a number.
# multi_number = int(input("Please input a number: "))
# for mul in range (1,11):
#     cheng=multi_number*mul
#     print (str(multi_number) +'*'+ str(mul)+ '='+str(cheng))


# 44. Write a Python program to construct the following pattern, using a nested loop number.
# for pattern in range (1,10):
    # #for pattern1 in range(pattern):
    #     print (str(pattern)* pattern)
