#7accept filename from user and print the extension

filename=input("Enter the file name: ")
f_extns=filename.split(".")
print ("The file's extension is: " + f_extns[-1])

#8 print first and last color of this list
color_list = ["Red","Green","White" ,"Black"]
print (color_list[0],color_list[-1])

#get the exam date from the tuple. tuple can not be called. %i/%i/%i
exam_st_date = (11, 12, 2014)
print ("The exam date is: %i/%i/%i"%exam_st_date)

phone_number = (1,512,203,4818)
print ("4 items in tuple as a phone number: %i-%i-%i-%i"%phone_number)

#10 Write a Python program that accepts an integer (n) and computes the value of n+nn+nnn

n = int(input("Please Enter your integer: "))
single=int("%s" %n)
tenth=int("%s%s" % (n,n))
hundredth = int("%s%s%s"%(n,n,n))
print(single+tenth+hundredth)

#11  Write a Python program to print the documents (syntax, description etc.) of Python built-in function(s)
# this is to use docstring
print (abs.__doc__)
print(sum.__doc__)

#12 Write a Python program to print the calendar of a given month and year. Note : Use 'calendar' module.
import calendar
year = int(input("Enter year:"))
month = int(input ("Enter month:"))
print (calendar.month(year,month))


