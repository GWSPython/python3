# 13 Write a Python program to print the following here document
#so multi line string needs multi-quotation mark
print ("""a string that you "don't" have to escape
This
is a ....... multi-line
heredoc string --------> example""")

print("\n")
#14 Write a Python program to calculate number of days between two dates.
from datetime import date
start_date=date(1995,9,1)
end_date=date(1997,6,6 )
delta= end_date - start_date
print(delta.days)

print("\n")
#15 Write a Python program to get the volume of a sphere with radius 6.
radius=6
from math import pi
V=4/3*pi*radius**3
print(V)

print("\n")
#16 Write a Python program to get the difference between a given number and 17,
# if the number is greater than 17 return double the absolute difference
num = int(input("Please enter a number: "))
if num>17:
    print(abs(num-17))
else:
    print(17-num)

print("\n")
#17 Write a Python program to test whether a number is within 100 of 1000 or 2000.
num = int(input("Please enter a number: "))
if num>900 and num <1100:
    print("Number is within 100 of 1000.")
elif num>1900 and num<2100:
    print("Number is within 100 of 2000.")
else:
    print("Number is not within 100 of 1000 or 2000.")

#Sample solution for #17
def near_thousand(n):
    return ((abs(1000 - n) <= 100) or (abs(2000 - n) <= 100))


print(near_thousand(1000))
print(near_thousand(900))
print(near_thousand(800))
print(near_thousand(2200))

print("\n")
#18 Write a Python program to calculate the sum of three given numbers, if the values are equal then return thrice of their sum.
a1=int(input("Please enter first number: "))
a2=int(input("Please enter second number: "))
a3=int(input("Please enter third number: "))
sum=a1+a2+a3
print(sum)
if a1 == a2 == a3:
    print(sum*3)

print("\n")
#using function
def sum_thrice(a1,a2,a3):
    sum=(a1+a2+a3)

    if a1 == a2 == a3:
        print(sum*3)
    return sum

sum_thrice(1,2,3)
sum_thrice(6,6,6)

print("\n")
#19 Write a Python program to get a new string from a given string
# where "Is" has been added to the front. If the given string already begins with "Is" then return the string unchanged

def given_string(str):
    if len(str)>2 and str[:2] == "Is":
        print(str)
    else:
        print("Is "+str)
given_string("I am sleepy")
given_string("Is it a cat?")

print("\n")
#20 Write a Python program to get a string which is n (non-negative integer) copies of a given string.
def string(str,n):
    for i in range(n):
        print (str)

string("TOO AWESOME!",3)


