# 1
print("Twinkle, twinkle, little star,")
#print("\n")
print("        How I wonder what you are!")
print("                Up Above the world so high,")
print("                Like a diamond in the sky.")
print("Twinkle, twinkle, little star,")
print("        How I wonder what you are")

print("\n")

#sample solution using /n and /t
print("Twinkle, twinkle, little star,\n\tHow I wonder what you are!\n\t\tUp above the world so high,\n\t\tLike a diamond inteh sky.\nTwinkle, twinkle, little star,\n\tHow I wonder what you are.")

print("\n")

#2 get python version (from internet)
import sys
print ("My Python Version:")
print(sys.version)
print("My Version Info:")
print(sys.version_info)

print("\n")
#3 current date and time (from internet)
import datetime
now = datetime.datetime.now()
print("current date and time is:")
print(now.strftime("%Y-%m-%d %H:%M:%S"))

print("\n")
#4 from even odd input method and formula from internet
def radius_and_area():
    print("Please Enter your radius")
    r = int(input().strip())
    print("Number Entered is {:d}".format(r))

    from math import pi
    a = pi*r**2
    print("The area of the circle with radius " + str(r)+ " is: " + str(pi*r**2))

radius_and_area()

print("\n")
#5 take user first and last name and print in reverse order with space in between

firstname = input(("please enter your first name:"))
lastname = input("Please enter your last name:")
print(lastname + " " + firstname)

print("\n")
#6 accepts a sequence of comma-separated number and generate list and tuple. Input really comes in handy.

n = input("Please enter comma separated numbers: ")
list = n.split(",")
tuple=tuple(list)
#I used '+ list' and it didn't work
print("List:" ,list)
print("Tuple: " ,tuple)

