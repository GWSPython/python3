# # 1.Write a Python class to convert an integer to a roman numeral.
#
# # list index out of range error. Incomplete. Very difficult according to Google search.  recursive..etc complex concept.
# class python_convert:
#     def convert_roman (self,num):
#         val = [1000,900,500,400,100,90,50,40,10,9,5,4,1]
#         syb = ["M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"]
#         roman_num=''
#         i=0
#         while num>0:
#             for _ in range (num // val[i]):
#                 roman_num += syb[i]
#                 num -= val[i]
#             i+=1
#         return roman_num
#
# print(python_convert().convert_roman(1))
# print(python_convert().convert_roman(4000))
# print(python_convert().convert_roman(888))

# # 2. Write a Python class to convert a roman numeral to an integer.
# class number_convert:
#     def convert_number (self,roman):
#         val = [1000,900,500,400,100,90,50,40,10,9,5,4,1]
#         syb = ["M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"]
#         numeric_num=0
#         j=0
#         i=0
#         input_list = list(roman)
#         if roman!='':
#             for i in range (0,13):
#                 for j in range (len(input_list)):
#                     if input_list[j] == syb[i]:
#                         numeric_num += val[i]
#                 j+=1
#             i+=1
#         else:
#             print ("Please put in your roman number.")
#         return numeric_num
#
# print(number_convert().convert_number('M'))
# print(number_convert().convert_number('V'))
# print(number_convert().convert_number('DCCCLXXXVIII'))

# 3. Write a Python class to find validity of a string of parentheses, '(', ')', '{', '}', '[' and '].
# These brackets must be close in the correct order, for example "()" and "()[]{}" are valid but "[)", "({[)]" and "{{{" are invalid
class parentheses_solution:
    def correct_parentheses(self,key):
        stack, parentheses_char = [], {"(": ")", "{": "}", "[": "]"}
        for parentheses in key:
            if parentheses in parentheses_char:
                stack.append(parentheses)
            elif len(stack)==0 or parentheses_char[stack.pop()] != parentheses:
                return False
        return len(stack)==0

print (parentheses_solution().correct_parentheses("(){}[]"))
print (parentheses_solution().correct_parentheses("(){[]"))
print (parentheses_solution().correct_parentheses("[]"))