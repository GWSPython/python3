# 11. Write a Python program which takes two digits m (row) and n (column) as input and generates a two-dimensional array.
# The element value in the i-th row and j-th column of the array should be i*j.

# # 12. Write a Python program that accepts a sequence of lines (blank line to terminate) as input
# # and prints the lines as output (all characters in lower case).
# line = []
# while True:
#     l = input("Please put lines of words you want to convert to upper case: ")
#     if l:
#         line.append(l.upper())
#     else:
#         break;
#
# for l in line:
#     print(l)

# 13. Write a Python program which accepts a sequence of comma separated 4 digit binary numbers
# as its input and print the numbers that are divisible by 5 in a comma separated sequence.

# number_divided_by_5 = []
# num_div_5 = [x for x in input().split(',')]
# for p in num_div_5:
#     x=int(p)
#     number_divided_by_5.append(p)
#     if x%5==0:
#         print(','.join(number_divided_by_5))

# # 14. Write a Python program that accepts a string and calculate the number of digits and letters.
# string_dig_letter = input ("please input your string: ")
# d=l=0
# for s in string_dig_letter:
#     if s.isdigit():
#         d=d+1
#     elif s.isalpha():
#         l=l+1
#     else:
#         pass
# print ("The string has ", d, "numbers.")
# print ("The string has ", l , "letters.")

# # 15 15. Write a Python program to check the validity of password input by users.
# # Validation :
# #
# # At least 1 letter between [a-z] and 1 letter between [A-Z].
# # At least 1 number between [0-9].
# # At least 1 character from [$#@].
# # Minimum length 6 characters.
# # Maximum length 16 characters.
# import re
# p=True
# password=input("Please input your password: ")
# while p:
#     if len(password)<6 and len(password)>16:
#         break
#     if re.search('[a,z]',password):
#         break
#     elif re.search('[A,Z]',password):
#         break
#     elif not re.search('[0-9]',password):
#         break
#     elif not re.search('[$#@]',password):
#         break
#     else:
#         p=False
#         print("Valid Password.")
#         break
# if p:
#     print("Invalid password.")

# # 16. Write a Python program to find numbers between 100 and 400 (both included)
# # where each digit of a number is an even number. The numbers obtained should be printed in a comma-separated sequence.
#
# all_dig_even=[]
# for num_all_digit_even in range (100,401):
#     all_digit=str(num_all_digit_even)
#     if int(all_digit[0])%2==0 and int(all_digit[1])%2==0 and int(all_digit[2])%2==0:
#         all_dig_even.append(all_digit)
#     else:
#         pass
#
# print(','.join(all_dig_even))

# # 17. Write a Python program to print alphabet pattern 'A'.
# pattern_string=""
# for row in range (0,7):
#     for column in range (0,6):
#         if ((column == 1 or column == 5) and row!=0) or (row==0 or row==3) and (column>1 and column<5):
#             pattern_string=pattern_string+"*"
#         else:
#             pattern_string=pattern_string+" "
#     pattern_string=pattern_string+"\n"
# print(pattern_string)

# # 18. Write a Python program to print alphabet pattern 'D'
# d_string=""
# for row in range (0,7):
#     for column in range (0,6):
#         if (row==0 and column!=5 and column!=4)or (row ==6 and column!=5 and column!=4):
#             d_string=d_string+"*"
#         elif (row!=0 and row!=6) and (column==0 or column==4):
#             d_string=d_string+"*"
#         else:
#             d_string=d_string+" "
#     d_string=d_string+"\n"
# print (d_string)

# # 19. Write a Python program to print alphabet pattern 'E'
# e_string = ""
# for row in range (0,7):
#     for column in range (0,5):
#         if (row==0 or row ==6) or (row ==3 and column in range (0,4)) or column==0:
#             e_string=e_string+"*"
#         else:
#             e_string=e_string+" "
#     e_string=e_string+"\n"
# print(e_string)

# # 20. Write a Python program to print alphabet pattern 'G'.
# g_string = ""
# for row in range (0,7):
#     for column in range (0,5):
#         if ((row ==0 or row ==6)or ((row in range (1,3) or row in range (4,6)) and (column ==0 or column ==5)) or (row==3 and column !=1)):
#             g_string=g_string+"*"
#         else:
#             g_string=g_string+" "
#     g_string=g_string+"\n"
# print (g_string)

# # 21. Write a Python program to print alphabet pattern 'L'
# l_string=""
# for row in range (0,7):
#     for column in range (0,5):
#         if column==0 or row == 6:
#             l_string=l_string+"*"
#         else:
#             l_string=l_string+" "
#     l_string=l_string+"\n"
# print (l_string)

# # 22. Write a Python program to print alphabet pattern 'M'
# m_string=""
# for row in range (0,7):
#     for column in range (0,6):
#         if (column ==0 or column ==4) or (row==2 and (column==1 or column==3)) or (row==3 and column==2):
#             m_string=m_string+"*"
#         else:
#             m_string=m_string+" "
#     m_string=m_string+"\n"
# print(m_string)

# # 23. Write a Python program to print alphabet pattern 'O'.
# o_string=""
# for row in range (0,7):
#     for column in range (0,6):
#         if (((row==0 or row ==6) and (column==1 or column==2 or column ==3)) or ((row!=0 and row!=6) and(column==0 or column==4))):
#             o_string=o_string+"*"
#         else:
#             o_string=o_string+" "
#     o_string=o_string+"\n"
# print(o_string)

# # 24. Write a Python program to print alphabet pattern 'P'
# p_string=""
# for row in range (0,7):
#     for column in range (0,6):
#         if (column==0 or ((row==0 or row==3)and column!=4)or ((row==1 or row==2)and column==5)):
#             p_string=p_string+"*"
#         else:
#             p_string=p_string+" "
#     p_string=p_string+"\n"
# print(p_string)

# # 25. Write a Python program to print alphabet pattern 'R.
# r_string=""
# for row in range (0,7):
#     for column in range (0,5):
#         if column==0 or ((row==0 or row==3) and column!=4) or ((row==1 or row==2 or row==6) and column==4) or (row==4 and column==2)or (row==5 and column==3):
#             r_string=r_string+"*"
#         else:
#             r_string=r_string+" "
#     r_string=r_string+"\n"
# print(r_string)

# #26. Write a Python program to print the following patterns "S"
# s_string=""
# for row in range (0,7):
#     for column in range (0,5):
#         if ((row==0 or row==3 or row ==6) and column in range (1,4)) or ((row==0 or row==4 or row==5) and column ==4)or ((row==1 or row==2 or row==6)and column==0):
#             s_string=s_string+"*"
#         else:
#             s_string=s_string+" "
#     s_string=s_string+"\n"
# print(s_string)

# big_s_string = ""
# for row in range(0, 16):
#     if row in range(0, 3) or row in range(6, 9) or row in range(12, 15):
#         for column in range(0, 19):
#             big_s_string = big_s_string + "o"
#     elif row in range(3, 6):
#         for column in range(0, 4):
#             big_s_string = big_s_string + "o"
#     elif row in range(9, 12):
#         for column in range (0,15):
#             big_s_string=big_s_string+" "
#         for column in range(15, 19):
#             big_s_string = big_s_string + "o"
#     else:
#         big_s_string = big_s_string + " "
#     big_s_string = big_s_string + "\n"
# print(big_s_string)

# 27. Write a Python program to print alphabet pattern 'T'
# t_string=""
# for row in range (0,7):
#     for column in range (0,5):
#         if row==0 or (row!=0 and column==2):
#             t_string=t_string+"*"
#         else:
#             t_string=t_string+" "
#     t_string=t_string+"\n"
# print(t_string)

# # 28. Write a Python program to print alphabet pattern 'U'
# u_string = ""
# for row in range (0,7):
#     for column in range (0,5):
#         if row!=6 and (column ==0 or column ==4) or row==6 and column in range (1,4):
#             u_string=u_string+"*"
#         else:
#             u_string=u_string+" "
#     u_string=u_string+"\n"
# print (u_string)

## 29. Write a Python program to print alphabet pattern 'X'
# x_string = ""
# for row in range(0, 7):
#     for column in range(0, 5):
#         if ((row in range(0, 2) or row in range(5, 7)) and (column == 0 or column == 4)) or (
#                 (row == 2 or row == 4) and (column == 1 or column == 3)) or (row == 3 and column == 2):
#             x_string = x_string + "*"
#         else:
#             x_string = x_string + " "
#     x_string = x_string + "\n"
# print(x_string)

# 30. Write a Python program to print alphabet pattern 'Z'.
# z_string = ""
# for row in range(0, 7):
#     for column in range(0, 7):
#         if (row==0 or row==6) or (row==1 and column ==5) or (row==2 and column==4) or (row==3 and column==3) or (row==4 and column==2) or (row==5 and column==1):
#             z_string = z_string + "*"
#         else:
#             z_string = z_string + " "
#     z_string = z_string + "\n"
# print(z_string)

